package UnplannedTasks;

public class Task7 {

    private int x;
    private int y;

    public Task7(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void showX(){
        System.out.println(x);
    }

    public void showY(){
        System.out.println(y);
    }

    public int sum(){
        return x+y;
    }

    public int max(){
        if (x<y) return y;
        return x;
    }
}
