package UnplannedTasks.Task10;

public class BookOfSubscribers {

    private int count;
    private Subscriber[] arrayOfSubscribes = new Subscriber[0];

    public void add(Subscriber subscriber){

        if (count == arrayOfSubscribes.length) resizeBook(arrayOfSubscribes.length + 1);
        arrayOfSubscribes[count++] = subscriber;
    }

    public void remove(int index){
        if (index > -1){
            for (int i = index; i < arrayOfSubscribes.length-1; i++) {
                arrayOfSubscribes[i] = arrayOfSubscribes[i+1];
            }
            arrayOfSubscribes[count-1] = null;
            count--;
            if (count > 0 && count < arrayOfSubscribes.length) resizeBook(count);
        }
    }

    public int getIndex(int id){
        for (int i = 0; i < arrayOfSubscribes.length; i++) {
            if (arrayOfSubscribes[i].getId() == id) return i;
        }
        return -1;
    }

    private void resizeBook(int lenght){
        Subscriber[] tmpBook = new Subscriber[lenght];
        System.arraycopy(arrayOfSubscribes,0,tmpBook,0,count);
        arrayOfSubscribes = tmpBook;

    }

    public Subscriber get(int id){
        for (Subscriber sub: arrayOfSubscribes) {
            if (sub.getId() == id) return sub;
        }
        return null;
    }

    public void showUrbanNegotiationTime(){
        for (Subscriber sub: arrayOfSubscribes) {
            if (sub.getUrbanNegotiationTime() > 0) System.out.printf("%s\n", sub.toString());
        }
    }

    public void showInternationalNegotiationTime(int timeLimit){
        for (Subscriber sub: arrayOfSubscribes) {
            if (sub.getTimeOfInternationalNegotiations() > timeLimit) System.out.printf("%s\n", sub.toString());
        }
    }

    public void showAllUsers(){
        for (Subscriber sub: arrayOfSubscribes) {
            System.out.printf("%s\n", sub.toString());
        }
    }



}
