package UnplannedTasks.Task10;

import java.util.Scanner;

public class Subscriber {

    private int id;
    private String lastName;
    private String firstName;
    private String patronymic;
    private String address;
    private int numOfCreditCard;
    private int debit;
    private int credit;
    private int urbanNegotiationTime;
    private int timeOfInternationalNegotiations;

    Scanner in = new Scanner(System.in);


    public Subscriber(int id, String lastName, String firstName, String patronymic, String address, int numOfCreditCard) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.address = address;
        this.numOfCreditCard = numOfCreditCard;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumOfCreditCard() {
        return numOfCreditCard;
    }

    public void setNumOfCreditCard(int numOfCreditCard) {
        this.numOfCreditCard = numOfCreditCard;
    }

    public int getDebit() {
        return debit;
    }

    public void setDebit(int debit) {
        this.debit = debit;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public int getUrbanNegotiationTime() {
        return urbanNegotiationTime;
    }

    public void setUrbanNegotiationTime(int urbanNegotiationTime){
        try {
            if (urbanNegotiationTime < 0) throw new Exception("Invalid value");
            else this.urbanNegotiationTime = urbanNegotiationTime;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Input other value of time");
            setUrbanNegotiationTime(in.nextInt());
        }
    }

    public int getTimeOfInternationalNegotiations() {
        return timeOfInternationalNegotiations;
    }

    public void setTimeOfInternationalNegotiations(int timeOfInternationalNegotiations) {
        try {
            if (timeOfInternationalNegotiations < 0) throw new Exception("Invalid value");
            else this.timeOfInternationalNegotiations = timeOfInternationalNegotiations;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Input other value of time");
            setTimeOfInternationalNegotiations(in.nextInt());
        }
    }

    @Override
    public String toString() {
        return "ID: " + id + " | " + lastName + " | " + firstName + " | " + address + " | " + numOfCreditCard + " | " + urbanNegotiationTime + " | " + timeOfInternationalNegotiations;
    }
}
