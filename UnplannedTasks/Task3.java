package UnplannedTasks;

public class Task3 {


    public static char[] format(String str){

        boolean flagStart = true;
        boolean flag = true;
        boolean flagWord = false;
        char[] array = str.toCharArray();
        for (int i = 0; i < array.length-1; i++) {
            if (array[i] == ' ' && array[i+1]!= ' '){
                array[i+1]= (String.valueOf(array[i+1]).toUpperCase()).charAt(0);
                if (flagStart) {
                    flagWord = true;
                    array = deleteSpace(array,i);
                } else if ((((int)array[i+1] >= 33) && ((int) array[i+1]<= 47)) || (((int)array[i+1] >= 58) && ((int) array[i+1]<= 64)) || (((int)array[i+1] >= 91) && ((int) array[i+1]<= 96)) || (((int)array[i+1] >= 123) && ((int) array[i+1]<= 125))){
                        array = deleteSpace(array,i);
                    flagWord = false;
                }
                flag = false;
                flagStart = false;


            }else if (array[i]!= ' ' && array[i+1] == ' ') {
                flag = true;
                flagWord = false;
            }
            else if(flag){ array = deleteSpace(array,i);
                i--;

            }  else if (flagWord)array[i]= (String.valueOf(array[i]).toLowerCase()).charAt(0);
        }
        return array;
    }

    private static char[] deleteSpace(char[] str, int index){
        for (int i = index; i< str.length -1; i++){
            str[i] = str[i + 1];
        }
        str =  resize(str.length-1, str);

        return str;
    }

    private static char[] resize(int size, char[] str){
        char[] tmpStr = new char[size];
        System.arraycopy(str,0, tmpStr, 0, tmpStr.length);

        return tmpStr;
    }

    public static void show(char[] array){
        for (char arg: array) {
            System.out.print(arg);
        }
    }
}
