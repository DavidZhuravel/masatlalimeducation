package UnplannedTasks;

import java.util.Scanner;

public class Task1 {

    private int num;
    private int numOfAttempts;

    public int getNum() {
        return num;
    }

    public void setNum() {
        this.num = (int)(Math.random()*5);
    }

    public int getNumOfAttempts() {
        return numOfAttempts;
    }

    public void setNumOfAttempts() {
        this.numOfAttempts = (int)(Math.random()*5);
    }

    public String game(){

        Scanner in = new Scanner(System.in);
        setNum();
        setNumOfAttempts();

        int guess;
        while (numOfAttempts>0){
            System.out.println("------------------------------------------");
            System.out.println("You have " + numOfAttempts + " of attempts");
            System.out.println("Input your guess:");
            guess = in.nextInt();
            if (guess < getNum()) System.out.println("hidden number is greater");
            else if (guess > getNum()) System.out.println("hidden number is less");
            else return "You win!:)";
            numOfAttempts--;
        }
        return "You lose!:(";
    }
}
