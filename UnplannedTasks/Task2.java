package UnplannedTasks;

public class Task2 {

    private int[] array = {1,2,3,4};
    private int[] tmpArray = array.clone();

    private void swap(int a, int b, int[] array){
        array[b] = (array[a] + array[b]) - (array[a] = array[b]);
    }

    public void randomSort(){
        int tmpCount;
        for (int count = 0; count < array.length; count++) {
            tmpCount = count;
            while (tmpCount == count && tmpArray[tmpCount] == array[tmpCount] ) {
                tmpCount = (int) (Math.random() * (array.length - 1));

            }
            swap(count,tmpCount,array);
        }
    }
    public void show(){
        for (int num: array) {
            System.out.printf("%d ", num);
        }
    }
}
