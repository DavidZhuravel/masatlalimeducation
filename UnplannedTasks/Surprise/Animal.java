package UnplannedTasks.Surprise;

public abstract class Animal {

    private int numOfFood;
    private String typeOfFood;
    private String name;


    public int getNumOfFood() {
        return numOfFood;
    }

    public void setNumOfFood(int numOfFood) {
        this.numOfFood = numOfFood;
    }

    public String getTypeOfFood() {
        return typeOfFood;
    }

    public void setTypeOfFood(String typeOfFood) {
        this.typeOfFood = typeOfFood;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void showNumOfFood();

    @Override
    public String toString() {
        return getClass().getSimpleName() +": "+  name + " eats " + numOfFood + " of " + typeOfFood;
    }
}
