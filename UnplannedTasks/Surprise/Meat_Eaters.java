package UnplannedTasks.Surprise;

public class Meat_Eaters extends Animal {

    public Meat_Eaters(String name) {
        setName(name);
        setNumOfFood(5);
        setTypeOfFood("meat");
    }

    @Override
    public void showNumOfFood() {
        System.out.println( getName() + " need "+getNumOfFood() + " of " + getTypeOfFood());
    }
}
