package UnplannedTasks.Surprise;

public class Omnivores extends Animal {

    public Omnivores( String name) {
        setName(name);
        setNumOfFood(9);
        setTypeOfFood("all types of food");
    }

    @Override
    public void showNumOfFood() {
        System.out.println( getName()+ " need "+getNumOfFood() + " of " + getTypeOfFood());
    }
}
