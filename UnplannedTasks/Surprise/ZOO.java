package UnplannedTasks.Surprise;

import java.util.Scanner;

public class ZOO {

    public static void main(String[] args) {

        AnimalList animalList = new AnimalList();
        animalList.add(new Herbivores("Cow"));
        animalList.add(new Omnivores("Human"));
        animalList.add(new Meat_Eaters("Rex"));
        animalList.showAllAnimalList();
        System.out.println("---------------");
        animalList.sortByNumOfFood();
        animalList.showAllAnimalList();
        System.out.println("________________");
        animalList.showFiveNameOfAnimalsInSortedList();
        System.out.println("________________");
        animalList.show3LastValueOfAllAnimalList();
    }
}
