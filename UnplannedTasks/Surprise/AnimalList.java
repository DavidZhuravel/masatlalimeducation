package UnplannedTasks.Surprise;

public class AnimalList {

    private Animal[] arrayList = new Animal[0];
    private int count;

    public void add(Animal animal){
        arrayList = resize(arrayList.length + 1);
        arrayList[count++] = animal;

    }

    private Animal[] resize(int lenght){
        Animal[] tmpList = new Animal[lenght];
        System.arraycopy(arrayList, 0,tmpList,0,count);
        return tmpList;
    }

    public int getIndex(String name){
        for (int i = 0; i < count; i++) {
            if (arrayList[i].getName().equals(name)) return i;
        }
        return -1;
    }

    public void delete(String name) {

        if (getIndex(name) > - 1) {
            for (int i = getIndex(name); i < count - 1; i++) {
                arrayList[i] = arrayList[i + 1];
            }
            arrayList[count-1] = null;
            count--;
            if (arrayList.length > 0 && count < arrayList.length) {
                arrayList = resize(count);
            }
        } else {
            System.out.println("Don`t find the element");
        }
    }

    public void delete(int index) {
        for (int i = index; i < count - 1; i++) {
            arrayList[i] = arrayList[i + 1];
        }
        arrayList[count--] = null;
        if (arrayList.length > 0 && count < arrayList.length) {
            arrayList = resize(count);
        }
    }

    public void showFiveNameOfAnimalsInSortedList(){
        sortByNumOfFood();
        int lenght;
        if (arrayList.length >= 5) lenght = 5;
        else lenght = arrayList.length;
        for (int i = 0; i < lenght; i++) {
            System.out.println(arrayList[i].getName());
        }
    }

    public void show3LastValueOfAllAnimalList(){
        for (Animal animal : arrayList) {
            animal.showNumOfFood();
        }
    }


    public void showAllAnimalList(){
        for (Animal animal : arrayList) {
            System.out.println(animal.toString());
        }
    }

    private void sortByName(int firstIndex, int secondIndex ){

        for (int k = 0; k < arrayList[getIndex(minWord(arrayList[firstIndex].getName(),arrayList[secondIndex].getName()))].getName().length(); k++) {
            if ( (int)arrayList[firstIndex].getName().toLowerCase().charAt(k) == (int) arrayList[secondIndex].getName().toLowerCase().charAt(k)) continue;
            else if ((int)arrayList[firstIndex].getName().toLowerCase().charAt(k) > (int) arrayList[secondIndex].getName().toLowerCase().charAt(k)){
                swap(firstIndex, secondIndex);
                break;
            }
        }
}

    public void sortByNumOfFood(){
        for (int i = 0; i < count; i++){
            for (int j = i; j < count-1; j++) {
                if (arrayList[j].getNumOfFood() >  arrayList[j+1].getNumOfFood()){
                    swap(j,j+1);
                    break;
                }else if (arrayList[j].getNumOfFood() ==  arrayList[j+1].getNumOfFood()){
                    sortByName(j,j+1);
                }
            }
        }
    }

    private void swap(int firstIndex, int secondIndex){
        Animal tmp = arrayList[firstIndex];
        arrayList[firstIndex] = arrayList[secondIndex];
        arrayList[secondIndex] = tmp;
    }

    private String minWord(String firstWord, String secondWord){
        if (firstWord.length() > secondWord.length()) return secondWord;
        return firstWord;
    }



}
