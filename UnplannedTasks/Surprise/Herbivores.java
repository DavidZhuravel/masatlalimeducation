package UnplannedTasks.Surprise;

public class Herbivores extends Animal {

    Herbivores(String name){
        setName(name);
        setNumOfFood((int)(Math.random() * 10 + 1));
        setTypeOfFood("plants");
    }

    @Override
    public void showNumOfFood() {
        System.out.println( getName() + " need "+getNumOfFood() + " of " + getTypeOfFood());
    }

}
