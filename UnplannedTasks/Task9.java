package UnplannedTasks;

import java.util.Scanner;

public class Task9 {

    private int hours;
    private int minutes;
    private int seconds;

    Scanner in = new Scanner(System.in);

    public Task9(int hours, int minutes, int seconds) {
        setTime(hours,minutes,seconds);
    }

    public int getHours() {
        return hours;
    }

    public void setHours (int hours) throws Exception{
        if (hours < 0|| hours>24) throw new Exception("Wrong value of hours");
        else this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) throws Exception{
        if (minutes<0|| minutes>60) throw new Exception("Wrong value of minutes");
        else this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) throws Exception {
        if (seconds<0|| seconds>60) throw new Exception("Wrong value of seconds");
        else this.seconds = seconds;
    }

    public void setTime(int hours, int minutes, int seconds){
        try {
            setHours(hours);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Input other value of hours:");
            setTime(in.nextInt(),minutes,seconds);
        }
        try {
            setMinutes(minutes);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Input other value of minutes");
            setTime(hours,in.nextInt(),seconds);
        }
        try {
            setSeconds(seconds);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Input other value of seconds");
            setTime(hours,minutes,in.nextInt());
        }
    }

    public void changeTime(int hours, int minutes, int seconds) throws Exception{

        if (hours < 0|| hours>24) throw new Exception("Wrong value of hours");
        if (minutes<0|| minutes>60) throw new Exception("Wrong value of minutes");
        if (seconds<0|| seconds>60) throw new Exception("Wrong value of seconds");

        if (getSeconds() + seconds >= 60) {
            if (getMinutes() + minutes + 1 >= 60){
                if (getHours() + hours + 1 >= 24){
                    setHours(getHours() + hours + 1 - 24);
                }else setHours(getHours() + hours + 1);
                setMinutes(getMinutes() + minutes + 1 - 60);
            }else setMinutes(getMinutes() + minutes +1);
            setSeconds(getSeconds() + seconds - 60);
        }else setSeconds(getSeconds() + seconds);
    }

    @Override
    public String toString() {
        return "Time: "+hours+":"+minutes+":"+seconds;
    }
}
