package UnplannedTasks.Task8;

import static java.lang.Math.*;
import java.util.Scanner;

public class Triangle {

    private Point median;
    private Point[] arrayOfPoints = new Point[3];

    public Triangle() {
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < arrayOfPoints.length; i++) {
            System.out.printf("For x%d input coordinates x through Enter\n",i+1);
            arrayOfPoints[i] = new Point(in.nextInt(), in.nextInt());
        }
        findMedian();
    }

    public Point getMedian() {
        return median;
    }

    public void setMedian(Point median) {
        this.median = median;
    }

    public double lenght(Point point1, Point point2){
        return sqrt(pow(point1.getX() - point2.getX(),2) + pow(point1.getY() - point2.getY(), 2));
    }

    public double squareOfTriangle(){
         return  (double)Math.abs((((arrayOfPoints[0].getX() - arrayOfPoints[2].getX())*(arrayOfPoints[1].getY()-arrayOfPoints[2].getY()))-((arrayOfPoints[1].getX()-arrayOfPoints[2].getX())*(arrayOfPoints[0].getY() - arrayOfPoints[2].getY()))))/2;
    }

    public double perimeter(){
        return lenght(arrayOfPoints[0],arrayOfPoints[1]) + lenght(arrayOfPoints[1],arrayOfPoints[2]) + lenght(arrayOfPoints[0], arrayOfPoints[2]);
    }

    private void findMedian(){
        int x = 0;
        int y = 0;
        for (Point arrayOfPoint : arrayOfPoints) {
            x += arrayOfPoint.getX() / 3;
            y += arrayOfPoint.getY() / 3;
        }
       setMedian(new Point(x,y));
    }

    public void showCoordinates(){
        for (Point point: arrayOfPoints) {
            System.out.printf("x:%d  y:%d\n", point.getX(), point.getY());
        }
    }

    public void showMedian(){
        System.out.printf("The coordinates of median (%d;%d)", median.getX(),median.getY());
    }
}
