package UnplannedTasks;

public class Task5 {

    public static void createMatrix(int n){
        int[][] array = new int[n][n];

        for (int i = 0; i < n; i++) {
            System.out.println();
            for (int j = 0; j < n; j++) {
                if (i == j) array[i][j] = 3;
                else if (i<j) array[i][j] = 2;
                else array[i][j] = 1;

                System.out.print(array[i][j] + " ");
            }
        }
    }
}
