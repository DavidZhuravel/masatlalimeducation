package lesson_30.Task_1;

public class CommonResourse {

    private dataCell[] data;
    private int jobs;
    private int countOfRead;



   synchronized private void dataIsChanged() {
        changedOfData++;
    }


    private int changedOfData;


    public CommonResourse(int size) {

        fillData(size);
        jobs = 0;
        countOfRead = -1;
    }

    private  void fillData(int size){
        data = new dataCell[size];
        for (int i = 0; i < size; i++) {
            data[i] = new dataCell(generateRandomString(),i);
        }
    }

    private String generateRandomString(){

        String string = "";
        for (int i = 0; i < 5; i++) {
            string += (char) (Math.random()*26 + 65);
        }
        return string;
    }

    public int getJobs() {
        return jobs;
    }

    synchronized   public int getCountOfRead() {
        return countOfRead;
    }

    synchronized void setJobs(int jobs) {
        this.jobs = jobs;
    }

    synchronized public void setCountOfRead(int countOfRead) {
        this.countOfRead = countOfRead;
    }

    synchronized dataCell getValueOfData(){
        countOfRead+=1;
        if (countOfRead == data.length){
            countOfRead=0;
            dataIsChanged();
        }
        if (!canChange()) return null;
        return data[countOfRead];

    }

    synchronized void jobDone(){jobs++;}

    synchronized dataCell[] getData(){
        dataCell[] tmpData = new dataCell[data.length];
        for (int i = 0; i < data.length; i++) {
            tmpData[i] = new dataCell(data[i].string, data[i].position);
        }
        return tmpData;
    }

    synchronized public dataCell getDataCell(int index){
        if (index < data.length) return data[index];
        return null;
    }

    public boolean canDo(){
        return countOfRead < data.length;
    }

  synchronized   public boolean canChange(){ return changedOfData <= 1;}

    class dataCell{

        private String string;
        private int position;

        public dataCell(String string, int position) {
            this.string = string;
            this.position = position;
        }

        public String getString() {
            return string;
        }

        public void setString(String string) {
            this.string = string;
        }

        public int getPosition() {
            return position;
        }
    }
}
