package lesson_30.Task_1;


public class Main {

    public static void main(String[] args) {

        CommonResourse commonResourse = new CommonResourse(4);
        Thread thread1 = new Thread(new ThreadForWorkWithData(commonResourse));
        Thread thread2 = new Thread(new ThreadForWorkWithData(commonResourse));
        Thread threadFile = new Thread(new ThreadForWorkWithFile(commonResourse));

        threadFile.setPriority(10);
        threadFile.start();
        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
