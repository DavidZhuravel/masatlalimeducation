package lesson_30.Task_1;

import java.io.*;

public class ThreadForWorkWithFile implements Runnable {

   private CommonResourse commonResourse;
   private int countOfBackup;

    public ThreadForWorkWithFile(CommonResourse commonResourse) {
        this.commonResourse = commonResourse;
        countOfBackup = 0;
    }

    @Override
    public void run() {

        String wayToFile = "D:\\MyFiles\\Java\\Test\\Lesson30\\Task1\\test";

        while (true){

            if (commonResourse.getJobs() >= (commonResourse.getData().length-1)){
                wayToFile += countOfBackup++ + ".txt";
                writeToFile(wayToFile);
                commonResourse.setJobs(commonResourse.getJobs() - (commonResourse.getData().length-1));
                wayToFile = "D:\\MyFiles\\Java\\Test\\Lesson30\\Task1\\test";

                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void writeToFile(String fileName){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileName), false))) {
            System.out.println("Start write to file: " + fileName);
            String res = "";
            CommonResourse.dataCell[] data = commonResourse.getData();
            for (int i = 0; i < data.length; i++) {
                res += data[i].getString() + "_";
                if (i%10 == 0 && i!= 0) res += "\r";
            }
            writer.write(res);
            writer.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("End write to file: " + fileName);
    }
}
