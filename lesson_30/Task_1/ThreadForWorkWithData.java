package lesson_30.Task_1;


public class ThreadForWorkWithData implements Runnable {

    private CommonResourse commonResourse;


    public ThreadForWorkWithData(CommonResourse commonResourse) {
        this.commonResourse = commonResourse;
    }

    @Override
    public void run() {

        try {

            while (commonResourse.canDo()) {
                String string;
                int index;

                if ((index = commonResourse.getValueOfData().getPosition())!= -1) {

                    System.out.println(
                            Thread.currentThread().getName() +
                                    " put the data on position: " +
                                    index
                    );
                    string = commonResourse.getData()[index].getString();
                    string = changeString(string);

                    Thread.sleep((int) (Math.random() * 3000));

                    System.out.println(string);
                    commonResourse.getDataCell(index).setString(string);
                    System.out.println(Thread.currentThread().getName() + " was end");
                    commonResourse.jobDone();
                }
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String changeString(String string) {
        String res = "";
        char[] charArr = string.toCharArray();
        int random = (int) (Math.random() * 26);
        for (int i = 0; i < charArr.length; i++) {
            if (charArr[i] + random <= 90) {
                res += (char) (charArr[i] + random);
            } else res += (char) (charArr[i] + (random - 26));
        }
        return res;
    }
}
