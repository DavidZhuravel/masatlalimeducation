package lesson_30;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        CommonResource commonResource = new CommonResource();
        commonResource.print();
        Thread first = new Thread(new ThreadForCalculate(commonResource));
        Thread second = new Thread(new ThreadForCalculate(commonResource));
        Thread third = new Thread(new ThreadForCalculate(commonResource));
        Thread saveThread = new Thread(new ThreadForSave(commonResource));
        first.start();
        second.start();
        third.start();
        saveThread.start();
        try {
            first.join();
            second.join();
            third.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        commonResource.print();
    }
}

class ThreadForSave implements Runnable {
    CommonResource commonResource;

    public ThreadForSave(CommonResource commonResource) {
        this.commonResource = commonResource;
    }

    @Override
    public void run() {
        String way = "D:\\MyFiles\\Java\\Test\\Lesson30\\Task1";
        int countOfFile = 0;
        String res = "";

        while (true) {
            System.out.println(
                    Thread.currentThread().getName()
                            + " comm.Jobs "
                            + commonResource.jobs);
            if (commonResource.jobs >= 5) {
                System.out.println(
                        Thread.currentThread().getName()
                                + " flag1");
                way += countOfFile;
                System.out.println(way);
                try (FileWriter fileWriter = new FileWriter(way)) {
                    res = commonResource.getDataVal();
                    fileWriter.write(res);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                countOfFile++;
                commonResource.jobs -= 5;
                way = "D:\\MyFiles\\Java\\Test\\Lesson30\\Task1";
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}

class ThreadForCalculate implements Runnable {

    CommonResource commonResource;

    public ThreadForCalculate(CommonResource commonResource) {
        this.commonResource = commonResource;
    }

    private String changeStr(String str) {
        String res = "!";
        int charCode = 0;
        for (int i = 0; i < str.length(); i++) {
            charCode = str.charAt(i) + 1;
            if (charCode > 90) {
                charCode -= 25;
            }
            res += (char) charCode;
        }
        return res;
    }

    @Override
    public void run() {
        Cell tmp;

        while (commonResource.canDo()) {
            if (commonResource.canDo()) {
                tmp = commonResource.getCell();
                System.out.println(
                        Thread.currentThread().getName() +
                                " " + tmp.val);
                tmp.val = changeStr(tmp.val);
                System.out.println(
                        Thread.currentThread().getName() +
                                " " + tmp.val);
                commonResource.jobDone();
                try {
                    Thread.sleep((long) (Math.random() * 5000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}

class CommonResource {
    Cell[] data;
    int countTotake;
    int jobs;

    CommonResource(int size) {
        data = new Cell[size];
        countTotake = 0;
        jobs = 0;
        for (int i = 0; i < data.length; i++) {
            data[i] = new Cell(
                    Generator.generateString(5), i);
        }
    }

    synchronized String getDataVal() {
        String res = "";
        for (int i = 0; i < data.length; i++) {
            res += (data[i].val + " ");
            if ((i + 1) % 5 == 0) {
                res += "\r\n";
            }
        }
        return res;
    }

    CommonResource() {
        this(50);
    }

    synchronized boolean canDo() {
        if (countTotake < data.length) {
            return true;
        }
        return false;
    }

    synchronized void jobDone() {
        jobs++;
    }

    synchronized Cell getCell() {
        return data[countTotake++];
    }

    void print() {
        for (int i = 0; i < data.length; i++) {
            System.out.print(i + " : " + data[i].val + " ");
            if ((i + 1) % 5 == 0) {
                System.out.println();
            }
        }
    }


}

class Cell {
    String val;
    int pos;

    public Cell(String val, int pos) {
        this.val = val;
        this.pos = pos;
    }
}

class Generator {
    static String generateString(int size) {
        Random random = new Random();
        String res = "";
        for (int i = 0; i < size; i++) {
            res += (char) (random.nextInt(25) + 65);
        }
        return res;
    }
}
