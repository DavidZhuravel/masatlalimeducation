package lesson_6;
import lesson_5.ArrayTaskPart1;

public class ArrayTaskPart2 {
    private static double[][] array = new double[][] {{1,-1,1},{2,5,3},{3,0,3}};
    private static  double[][] array2 = new double[][]{{1,1}, {2,0},{3,3}};

    public static void main(String[] args) {


        //ArrayTaskPart1.showMatrix(insertionSort(array));
        //ArrayTaskPart1.showMatrix(reverseInsertionSort(array));
        //sortByIncreasingTheAmountOfItemsInTheColumn(array);
        //sortByDescendingTheAmountOfItemsInTheColumn(array);
        //sortByIncreasingTheAmountOfEvenElementsInTheRow(array);
        //sortByDescendingTheAmountOfEvenElementsInTheRow(array);
        //countOfRowContainingPrimeNum(array);
        // countOfColumnContainingPrimeNum(array);
        multi2DMatrixs(array,array2);
    }

    public static void swapElem(int firstNum, int secondNum, double[] mass){
        double tmpNum;
        tmpNum = mass[firstNum];
        mass[firstNum] = mass[secondNum];
        mass[secondNum] = tmpNum;
    }

    public static void swapElem(int iIndexFirst, int jIndexFist, int iIndexSecond, int jIndexSecond, double[][] mass){
        double tmpNum;
        tmpNum = mass[iIndexFirst][jIndexFist];
        mass[iIndexFirst][jIndexFist] = mass[iIndexSecond][jIndexSecond];
        mass[iIndexSecond][jIndexSecond] = tmpNum;
    }

    public static void improvedBubbleSortForDescendingInRow(double[] mass, double[][] array){
        for (int i = mass.length-1; i >= 1; i--) {
            for (int j = 0; j < i ; j++)
            {
                if (mass[j]<mass[j+1]){
                    swapElem(j, j+1, mass);
                    for (int k = 0; k < mass.length ; k++) {
                        swapElem(j,k,j+1,k,array);
                    }
                }
            }
        }
    }

    public static void improvedBubbleSortForIncreaseIn(double[] mass, double[][] array){
        for (int i = mass.length-1; i >= 1; i--) {
            for (int j = 0; j < i ; j++)
            {
                if (mass[j]>mass[j+1]){
                    swapElem(j, j+1, mass);
                    for (int k = 0; k < mass.length ; k++) {
                        swapElem(j,k,j+1,k,array);
                    }
                }
            }
        }
    }

    public static void insertionSort(double[][] mass){

        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j< mass[i].length; j++){
                int tmpI = i;
                int tmpJ=0 ;
                if (j!=0) tmpJ = j-1;
                if (j==0 && i!=0){
                    tmpI = i - 1;
                    tmpJ = mass[tmpI].length-1;
                }
                while (mass[tmpI][tmpJ] > mass[i][j]) {
                    swapElem(tmpI,tmpJ,i,j,mass);

                    if (j >= 1) {
                        j--;
                        if (j==0) tmpJ = j;
                        else tmpJ = j -1;
                    }
                    if(j==0 && i!=0) {
                        i--;
                        j = mass[0].length-1;
                        tmpI = i;
                        tmpJ = j-1;
                    }
                    if (i == 0 && j == 0) break;
                }
            }
        }
    } // task 1

    public static void reverseInsertionSort(double[][] mass){

        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j< mass[i].length; j++){
                int tmpI = i;
                int tmpJ=0 ;
                if (j!=0) tmpJ = j-1;
                if (j==0 && i!=0){
                    tmpI = i - 1;
                    tmpJ = mass[tmpI].length-1;

                }
                while (mass[tmpI][tmpJ] < mass[i][j]) {
                    swapElem(tmpI,tmpJ,i,j,mass);
                    if (j >= 1) {
                        j--;
                        if (j==0) tmpJ = j;
                        else tmpJ = j -1;
                    }
                    if(j==0 && i!=0) {
                        i--;
                        j = mass[0].length-1;
                        tmpI = i;
                        tmpJ = j-1;
                    }
                    if (i == 0 && j == 0) break;
                }
            }
        }
    } //task2

    public static void sortByIncreasingTheAmountOfItemsInTheColumn(double[][] mass){
        double[] sumOfColums = new double[mass[0].length];
        for (int j = 0; j < mass[0].length; j++) {
            for (int i = 0; i < mass.length ; i++) {
                sumOfColums[j] += mass[i][j];
            }
        }
        for (int i = mass.length-1; i >= 1; i--) {
            for (int j = 0; j < i ; j++)
            {
                if (sumOfColums[j]>sumOfColums[j+1]){
                    swapElem(j, j+1, sumOfColums);
                    for (int k = 0; k < mass.length ; k++) {
                        swapElem(k,j,k,j+1,mass);
                    }
                }
            }
        }

        ArrayTaskPart1.showMatrix(mass);

    }

    public static void sortByDescendingTheAmountOfItemsInTheColumn(double[][] mass){
        double[] sumOfColums = new double[mass[0].length];
        for (int j = 0; j < mass[0].length; j++) {
            for (int i = 0; i < mass.length ; i++) {
                sumOfColums[j] += mass[i][j];
            }
        }
        for (int i = mass.length-1; i >= 1; i--) {
            for (int j = 0; j < i ; j++)
            {
                if (sumOfColums[j]<sumOfColums[j+1]){
                    swapElem(j, j+1, sumOfColums);
                    for (int k = 0; k < mass.length ; k++) {
                        swapElem(k,j,k,j+1,mass);
                    }
                }
            }
        }

        ArrayTaskPart1.showMatrix(mass);

    }

    public static void sortByIncreasingTheAmountOfEvenElementsInTheRow(double[][] mass){
        double[] sumOfEvenElementsInTheRow = new double[mass.length];
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length ; j++) {
                if ((j+1)%2 == 0 ) sumOfEvenElementsInTheRow[i] += mass[i][j];
            }
        }
        improvedBubbleSortForIncreaseIn(sumOfEvenElementsInTheRow,mass);

        ArrayTaskPart1.showMatrix(mass);

    }

    public static void sortByDescendingTheAmountOfEvenElementsInTheRow(double[][] mass){
        double[] sumOfEvenElementsInTheRow = new double[mass.length];
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length ; j++) {
                if ((j+1)%2 == 0 ) sumOfEvenElementsInTheRow[i] += mass[i][j];
            }
        }
        improvedBubbleSortForDescendingInRow(sumOfEvenElementsInTheRow,mass);


        ArrayTaskPart1.showMatrix(mass);

    }

    public static int countOfRowContainingPrimeNum(double[][] mass){

        int count = 0;
        boolean flag;
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                flag = true;
                for (int k = 2; k < mass[i][j] ; k++) {
                    if (mass[i][j]%k == 0) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    count++;
                    break;
                }
            }
        }
        System.out.println("countOfRowContainingPrimeNum "  + count);
        return count;
    }

    public static int countOfColumnContainingPrimeNum(double[][] mass){

        int count = 0;
        boolean flag;
        for (int j = 0; j < mass.length; j++) {
            for (int i = 0; i < mass.length; i++) {
                flag = true;
                for (int k = 2; k < mass[i][j] ; k++) {
                    if (mass[i][j]%k == 0) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    count++;
                    break;
                }
            }
        }
        System.out.println("countOfColumnContainingPrimeNum "  + count);
        return count;
    }

    public static void multi2DMatrixs(double[][] array1, double[][] array2){
        double[][] result = new double[array1.length][array2[0].length];
        for (int k = 0; k < array1.length; k++) {
            System.out.println();
            for (int i = 0; i < array2[0].length ; i++) {
                for (int j = 0; j < array1.length; j++) {
                    result[k][i] += array1[k][j] * array2[j][i];
                }
                System.out.print(result[k][i] + " ");
            }
        }
    }
}
