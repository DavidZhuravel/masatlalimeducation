package lesson_23;

public class Warrior extends Unit implements Comparable<Warrior>{

    private int damage;

    public Warrior(String name, int healght, int damage) {
        setName(name);
        setHealth(healght);
        this.damage = damage;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }


    @Override
    public int compareTo(Warrior o) {
        if (damage> o.getDamage()) return 1;
        else if (damage<o.getDamage()) return -1;
        else return 0;
    }

    @Override
    public String toString() {
        return   getName()+
                " has health=" + getHealth() +
                " and damage=" + damage + "\n";
    }
}
