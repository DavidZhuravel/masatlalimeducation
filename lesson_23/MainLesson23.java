package lesson_23;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.TreeSet;
import static java.lang.Math.*;

public class MainLesson23 {

    public static void main(String[] args) {

        TreeSet<Warrior> treeSet = new TreeSet<>();
        String[] names;

        names = getData("D:\\MyFiles\\Java\\Test\\Lesson25Test\\Name.txt");
        for (int i = 0; i < names.length; i++) {
            treeSet.add(new Warrior(names[(int)(random()*names.length)], (int)(random()*100),(int)(random()*100)));
        }

        putDataTXT(treeSet,"D:\\MyFiles\\Java\\Test\\Lesson25Test\\ListOfWarrior2.txt");



       /* System.out.println("------------------------------------------------");

        TreeSet<Magician> treeSet1 = new TreeSet<>();
        treeSet1.add(new Magician("John",100,56));
        treeSet1.add(new Magician("Ostin", 99, 73));
        treeSet1.add(new Magician("Jack", 65, 42));

        for (Magician magician : treeSet1) {
            System.out.println(magician);

        }

        System.out.println("------------------------------------------------");
        Comparator<Knight> compr = new KnightComparator();
        TreeSet<Knight> treeSet2 = new TreeSet<>(compr);
        treeSet2.add(new Knight("John",35,56,78));
        treeSet2.add(new Knight("Ostin", 99, 73,65));
        treeSet2.add(new Knight("Jack", 65, 42,88));

        for (Knight knight : treeSet2) {
            System.out.println(knight);

        }*/
    }

    public static String[] getData(String url){
        String[] arrayString = null;
        try (FileInputStream inputStream = new FileInputStream(url)){
            byte[] arrayBt = new byte[inputStream.available()];
            String string = "";
            inputStream.read(arrayBt,0,arrayBt.length);
            for (int i = 0; i < arrayBt.length; i++) {
                string += (char)arrayBt[i];
            }
            arrayString = string.split(" ");
            return arrayString;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayString;
    }

    public static void putData(TreeSet<? extends Unit> treeSet, String url){

        String result = "";

        try (FileOutputStream outputStream = new FileOutputStream(url)){
            for (Unit unit : treeSet) {
                result += unit + "\n";
            }

            outputStream.write(result.getBytes(),0,result.getBytes().length);

        } catch (IOException ex){
            ex.printStackTrace();
        }
    }

    public static void putDataTXT(TreeSet<? extends Unit> treeSet, String url){

        try (FileWriter writer = new FileWriter(url)){
            for (Unit unit : treeSet) {
                writer.write("Hello world! \n");
            }

        } catch (IOException ex){
            ex.printStackTrace();
        }
    }
}
