package lesson_23;

import lesson_23.Knight;

import java.util.Comparator;

public class KnightComparator implements Comparator<Knight> {

    @Override
    public int compare(Knight o1, Knight o2) {
        int tmpHealth1 = o1.getHealth();
        int tmpHealth2 = o2.getHealth();
        while (tmpHealth1 > 0 && tmpHealth2 > 0){
            tmpHealth1 -= (o1.getHealth()-(o2.getDamage()- (int)(Math.random() * (o1.getArmor() - Math.abs(o1.getArmor()-o2.getDamage())))));
            tmpHealth2 -= (o2.getHealth()-(o1.getDamage()- (int)(Math.random() * (o2.getArmor() - Math.abs(o2.getArmor()-o1.getDamage())))));
        }
        if (tmpHealth1 == 0) return 1;
        else if (tmpHealth2 == 0) return -1;
        else return 0;
    }
}