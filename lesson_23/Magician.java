package lesson_23;

public class Magician extends Unit implements Comparable<Magician>{

    private int mana;

    public Magician(String name, int health, int mana) {
        setName(name);
        setHealth(health);
        this.mana = mana;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    @Override
    public int compareTo(Magician o) {
        if (mana < o.getMana()) return 1;
        else if (mana > o.getMana()) return -1;
        else return 0;
    }

    @Override
    public String toString() {
        return "Magician{" + getName()+
                " has health=" + getHealth() +
                " and mana=" + mana +
                '}';
    }

}
