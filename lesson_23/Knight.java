package lesson_23;



public class Knight extends Warrior {

    private int armor;

    public Knight(String name, int health, int armor, int damage) {
        super(name,health,damage);
        this.armor = armor;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }



    @Override
    public String toString() {
        return "Knight{" + getName()+
                " has health=" + getHealth() +
                " and damage=" + getDamage() +
                " and armor=" + armor +
                '}';
    }


}
