package lesson_29.ProblemAboutPhilosophers;

import java.util.concurrent.Semaphore;

public class Dinner {

    public static void main(String[] args) {

        Semaphore sem = new Semaphore(2,true);

        for (int i = 1; i < 6; i++) {
            new Thread(new Philosopher(sem,i)).start();

        }
    }
}
