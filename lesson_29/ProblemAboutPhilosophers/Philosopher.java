package lesson_29.ProblemAboutPhilosophers;

import java.util.concurrent.Semaphore;

public class Philosopher implements Runnable {

    private int id;
    Semaphore sem;

    public Philosopher(Semaphore sem, int id) {

        this.sem = sem;
        this.id = id;
    }

    @Override
    public void run() {
        for (int i = 0; i<5; i++) {
            try {
                sem.acquire();
                System.out.println(id + "-й философ садится за стол");
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(id + "-й философ покидает стол");
            sem.release();
        }
    }
}
