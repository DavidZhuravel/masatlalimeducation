package lesson_29.BardsCompetition;

import java.util.Arrays;

public class Song {

    private String[] song = new String[0];

    public Song(){}

    public Song(String[] song) {
        this.song = song;
    }

    public Song(String song){
        this.song = song.split(" ");
    }

    public void setSong(String[] song) {
        this.song = song;
    }

    public void setSong(String song){
        this.song = song.split(" ");
    }




    public void addWordToSong(String word){
        if (song.length!=0) song = (Arrays.toString(song) + word).split(" ");
        else setSong(word);
    }

    public String getRandomWord(){
        return song[(int)(Math.random()*song.length)];
    }

    public void singAtSong(){
        for (String s : song) {
            System.out.println(s + " ");

        }
    }
}
