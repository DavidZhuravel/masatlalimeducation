package lesson_29.BardsCompetition;

import java.util.concurrent.Exchanger;
import java.util.concurrent.Phaser;
import java.util.concurrent.Semaphore;

public class Competition {

    public static void main(String[] args) {
        Phaser phaser = new Phaser(4);
        Exchanger<String> exchanger = new Exchanger<>();
        Semaphore semaphore = new Semaphore(3);
        Song song = new Song();
        new Thread(new Bard("q w e r t y", phaser,exchanger,semaphore)).start();
        new Thread(new Bard("a s d f g",phaser,exchanger,semaphore)).start();
        new Thread(new Bard("z x c v b",phaser , exchanger,semaphore)).start();
        /*new Thread(new Bard("p o i u y", phaser, exchanger,semaphore)).start();*/
song.singAtSong();
    }
}
