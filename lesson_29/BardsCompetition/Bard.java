package lesson_29.BardsCompetition;

import java.util.concurrent.Exchanger;
import java.util.concurrent.Phaser;
import java.util.concurrent.Semaphore;

public class Bard implements Runnable{

    private String[] song;
    private String newSong = "";


    Phaser phaser;
    Exchanger<String> exchanger;
    Semaphore semaphore;

    public Bard(String song, Phaser phaser, Exchanger<String> exchanger, Semaphore semaphore) {
        this.song = song.split(" ");
        this.phaser = phaser;
        this.exchanger = exchanger;
        //phaser.register();
        this.semaphore = semaphore;

    }


    @Override
    public void run() {
        for (int i = 0; i <3 ; i++) {


            try {
                // semaphore.acquire();
                String word = exchanger.exchange(song[(int) (Math.random() * song.length)]);
                newSong += word;
                //phaser.arriveAndAwaitAdvance();

                /*word = getWordToSong();
                newSong += word + " " + exchanger.exchange(word);
                //phaser.arriveAndDeregister();*/

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(newSong);
    }
}
