package lesson_15;

import java.util.Scanner;

public class Tasks {



    public static int task1(int x) {
        if (x == 0)
            return x;
        System.out.println(x);
        return task1(x - 1);


    }

    public static int task2(int a, int b) {

        if (a == b) {
            System.out.println(b);
            return b;
        }

        if (a > b) {
            System.out.println(a);
            return task2(a - 1, b);
        }

        System.out.println(a);
        return task2(a + 1, b);
    }

    public static int task3(int m, int n) {
        if (m == 0) {
            System.out.printf("m:%d  n:%d\n", m, n + 1);
            return n + 1;
        }
        if (m > 0 && n == 0) {
            System.out.printf("m:%d  n:%d\n", m, n);
            return task3(m - 1, 1);
        }
        System.out.printf("m:%d  n:%d\n", m, n);
        return task3(m - 1, task3(m, n - 1));

    }

    public static int task4(int x) {
        if (x == 0) return x;
        return (x % 10 + task4((x - x % 10) / 10));
    }

    public static void task5(int x) {


        System.out.print(x % 10 + " ");
        if (x > 9) task5((x - x % 10) / 10);
    }

    public static void task6(int x) {
        if (x > 9) task6(x / 10);
        System.out.println(" " + x % 10);
    }

    public static int task7(int x, int k) {

        if (x % k == 0) {
            System.out.println(k);
            return task7(x / k, 2);
        } else if (x != 1) task7(x, k + 1);

        return k;
    }

    public static String task8(String str) {
        if (str.charAt(0) == str.charAt(str.length() - 1) ) {
            if (str.length()>2)
                return task8(str.substring(1, str.length() - 1));
            else if (str.length() == 2) {
                if (str.charAt(0) == str.charAt(1)) return "Yes";
            } else return "Yes";
        }
        return "No";
    }

    public static void task9(){
        Scanner in = new Scanner(System.in);
        System.out.println("Input num:");
        int num = in.nextInt();
        if (num != 0){
            task9();
            if (num%2!=0) System.out.print(num +" ");
        }
    }

    public static  int task10(){
        Scanner in = new Scanner(System.in);
        System.out.println("Input num:");
        int num = in.nextInt();
        if (num!=0){
            int max = task10();
            if  (num< max)
                return max;
            else  return num;
        }
        return num;
    }
}

