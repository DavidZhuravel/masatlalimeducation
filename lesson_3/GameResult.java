package lesson_3;

public class GameResult {

    public static  byte result;
    public static void main(String[] args) {
        byte[][] mass =  {{0,0,1}, {0,1, 0}, {0,1,1}}; //входной массив крестики-нолики
        System.out.println("Winner:" + result(mass));
    }

    public static byte result(byte mass[][]){
        boolean X = false;
        boolean O = false;

        byte flagXZ = 0; // сумма значений по диагонале ((0;0)(2;2))
        byte flagYZ = 0; // сумма значений по диагонале ((2;0)(0;2))
        byte flagX;  // сумма значений по горизонтале
        byte flag0;   // сумма значений по вертикале

            for (int i = 0; i < mass.length; i++) {
                flagX=0;
                flag0=0;
                flagXZ += mass[i][i];
                flagYZ += mass[2-i][2-i];

                for (int j = 0; j < mass[0].length; j++) {

                    flagX += mass[i][j];
                    flag0 += mass[j][i];

                }
                if (flagX == 3 || flag0 == 3 ) {  // если сумма по горизонтале или вертикале равна 3, то победил Х
                    X = true;
                    break;

                }else if (flagX == 0 || flag0 == 0) { // если сумма по горизонтале или вертикале равна 0, то победил 0
                    O = true;
                    break;
                }
            }

            if (flagXZ == 0 || flagYZ == 0) { // если сумма по какой-либо диагонале равна 0, то победил 0
                O = true;
        }else if (flagXZ == 3 || flagYZ == 3){  // если сумма по какой-либо диагонале равна 3, то победил Х
                X = true;
        }

            if (X&&O!=true)
                return result = 1;
            else if (O&&X!=true)
                return  result = 0;
            else return result = 10; // если ничья
    }
}
