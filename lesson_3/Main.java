package lesson_3;

import train.BitCounting;

public class Main {
    public static void main(String[] args) {

       /* task1( 10);
        task2(1);
        task3(false);
        task4(49);
        task5(3);
        task6(4);
        task7(30);*/
       // task8(11);
        task9(7);
       // BitCounting.bitCouting(10);
    }

    public static void task1(int a){
        if(a == 10){
            System.out.println("True");
        }else System.out.println("False");
    }

    public static void task2(int a){
        if (a == 0){
            System.out.println("True");
        }else{
            System.out.println("False");
        }
    }

    public static void task3(boolean test){
        if (test){
            System.out.println("True");
        }else{
            System.out.println("False");
        }
    }

    public static void task4(int min){
        if (min<=15){
            System.out.println("First");
        }else if(min>15&&min<=30){
            System.out.println("Second");
        }else if(min>30&&min<=45){
            System.out.println("Third");
        }else System.out.println("Fourth");
    }

    public static void task5(int num){
        String result = null;
        switch (num){
            case 1 :  result = "Winter";
            break;

            case  2: result = "Spring";
            break;

            case 3: result = "Summer";
            break;

            case 4: result = "Autumn";
            break;
        }
        System.out.println(result);
    }

    public static void task6(int month){
        if (month==1 || month==2 || month == 12){
            System.out.println("Winter");
        }else if (month>=3 && month<=5){
            System.out.println("Spring");
        }else if (month>=6 && month<=8){
            System.out.println("Summer");
        }else if (month>=9 && month<=11){
            System.out.println("Autumn");
        }else System.out.println("Wrong");
    }

    public  static  void task7(int day){
        if (day<11){
            System.out.println("First");
        }else if (day>10 && day<21){
            System.out.println("Second");
        }else if (day>20 && day<=31){
            System.out.println("Third");
        }else System.out.println("Wrong");
    }

    public static  void task8(int num){
        boolean flag = false;
        for(int i = 1; i<num; i++){

            if (i>1 && i<num) {
                if ( num%i!=0 ){
                    flag = true;
                }else  {
                    flag = false;
                break;}
            }else  flag = true;
        }
        System.out.println("Simple number: " + flag);
    }

    public static void task9(int size){
        for(int i = size; i>0; i--){

            System.out.println();
            for(int j = i; j>0; j--){
                System.out.print("*");
            }

        }
    }


}
