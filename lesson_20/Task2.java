package lesson_20;

import java.util.ArrayList;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Task2 {

    public  void squareOfTriangle(short max) {

        for (short square = 2; square <= max; square++) {
            ArrayList<Triangle> array = new ArrayList<>();
            byte count = 0;
            short tmpB = 0;
            for (short a = 1; a<=square/2; a++){
                if (a == tmpB) continue;
                short b = (short)(2*square/a);
                short c = (short)(sqrt(pow(a,2)+pow(b,2)));
                if ( b>a && a*b/2==square && pow(c,2) == pow(a,2) + pow(b,2)){
                    tmpB = b;
                    array.add(new Triangle(a,b,c));
                    count++;
                }
            }
            if (count>1) for (Triangle triangle : array) {
                System.out.println(triangle.toString());
            }
        }
    }

    class Triangle{
        private short a;
        private short b;
        private short c;
        private short square;

        public Triangle(short a, short b, short c) {
            this.a = a;
            this.b = b;
            this.c = c;
            square = (short)(a*b/2);
        }

        @Override
        public String toString() {
            return "numbers of pifagore: " + a + " | " + b + " | " + c + " |square: " + square;
        }
    }

    public static void main(String[] args) {
        Task2 task2 = new Task2();
        task2.squareOfTriangle((short) 1000);
    }
}
