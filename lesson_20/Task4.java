package lesson_20;

public class Task4 {

    public int[][] array;
    private int i;
    private int j;
    private int count = 0;
    private int step = 0;
    private int num = 1;
    private boolean flag = true;

    public Task4(int i, int j) {
        this.i = i/2;
        this.j = j/2;
        array = new int[i][j];
        taskUlam();
        show();
    }

    public  void taskUlam(){
        array[i][j] = num;
        step++;
        left(++num);

    }

    public void left(int num){
        count++;

        for (int k = 0; k < step; k++) {
            if (j>0) {
                array[i][--j] = num++;

            }else {
                flag = false;
                break;}
        }
        if (count%2==0) step++;
        if (flag) down(num);
    }

    public void down(int num){

        count++;
        for (int k = 0; k < step; k++) {
            if ( i < array.length-1) {
                array[++i][j] = num++;
            } else {
                flag = false;
                break;}
        }
        if (count%2==0) step++;
        if (flag) right(num);
    }

    public void right(int num){

        count++;
        for (int k = 0; k < step; k++) {
            if (  j < array[i].length-1) {
                array[i][++j] = num++;
            }else {
                flag = false;
                break;}
        }
        if (count%2==0) step++;
        if (flag) up(num);
    }

    public void up(int num){
        count++;
        for (int k = 0; k < step; k++) {
            if ( i > 0) {
                array[--i][j] = num++;
            }else {
                flag = false;
                break;}
        }
        if (count%2==0) step++;
        if (flag) left(num);

    }

    public boolean simpleNum(int num){
        for (int k = 2; k < num; k++) {
            if (num%k==0)
                return false;
        }
        return true;
    }

    public void show(){
        for (int k = 0; k < array.length; k++) {
            System.out.println();
            for (int l = 0; l < array[k].length; l++) {
                if (simpleNum(array[k][l])) System.out.print(" * ");
                else System.out.print("   ");
            }
        }
    }
}
