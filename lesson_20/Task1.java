package lesson_20;
import static java.lang.Math.*;

public class Task1 {

    public static void numOfPifagore(int max){

        for (int c = 2; c <= max; c++) {
            for (int a = 1; a < c; a++) {
                int b = a;
                while (pow(c,2) >= pow(a,2) + pow(b,2)){
                    if (pow(c,2) == pow(a,2) + pow(b,2)){
                        System.out.println("numbers of pifagore: " + a +" | " + b +" | "+ c + " | " + simpleNum(a,b,c));
                        break;
                    }
                    b++;
                }
            }
        }
    }

    public static boolean simpleNum(int a, int b, int c){
        for (int i = 2; i < c; i++) {
          if (a%i ==0 && b%i==0 && c%i==0) return false;
        }
        return true;
    }

    public static void main(String[] args) {
        numOfPifagore(100);
    }


}
