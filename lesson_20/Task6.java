package lesson_20;

import static java.lang.Math.*;

public class Task6 {

    int[] array;
   int count;

    public void add(int num){
        array = resize(array.length);
        array[count++] = num;
    }

    public int[] resize(int lenght){
        int[] tmpArray = new int[lenght+1];
        System.arraycopy(array,0,tmpArray,0, lenght);
        return tmpArray;
    }

    public void show(){
        for (int i : array) {
            System.out.println(i);

        }
    }

    public void helper(){
        int count;
        int index = 1;
        for (int i = 0; i < index; i++) {
            count = 0;
            for (int j = index; j<array.length-1; j++) {

                if (array[i] == array[j]){
                    count++;
                    array[j] = (int) pow(array[j], (count+1));
                }else {
                    index += count;
                    if (i < index-1 )  add(array[j]*array[i]);

                break;}
            }
        }
    }

    public int sum(){
        int sum = 0;
        for (int i : array) {
            sum+=i;

        }
        return sum;
    }

    public void multipliers(int num, int startMultiplier){

        int startMulti = 2;
        while (true) {
            if (num%startMulti == 0){
                add(startMulti);
                num/=startMulti;
            }else if (startMulti < num){
                 startMulti += startMulti%2==0? 1 : 2;
            }else  {
                add(num);
            break;}
        }
    }

    public  void task6(int max){
        for (int i = 2; i< max; i++) {
            array = new int[0];
            count = 0;
            multipliers(i,2);
            helper();
            if (i == sum()) System.out.println("It`s a great number: " + i);
            else System.out.println(i);

        }
    }

    public static void main(String[] args) {
        long time = 0;

        for (int k = 0; k<5; k++) {
            long start = System.currentTimeMillis();
            Task6 task6 = new Task6();
            task6.task6(10000000);
            long finish = System.currentTimeMillis();
            System.out.println("Time of work program: " + (finish-start));
            time += finish-start;
        }
        System.out.println("Average time of work: " + (time/5));
    }
}
