package lesson_26;

public class Pair {

    Graph graph;
    Countess countess;

    public Pair(Graph graph, Countess countess) {
        this.graph = graph;
        this.countess = countess;
    }

    public void setPositionOfPair(int x, int y){
        graph.setXYposition(x,y);
        countess.setXYposition(x,y);
    }
}
