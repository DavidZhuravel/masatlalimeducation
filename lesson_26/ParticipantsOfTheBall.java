package lesson_26;

public class ParticipantsOfTheBall {

    private String name;
    private int x;
    private int y;

    public ParticipantsOfTheBall(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXYposition(int x, int y){
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "ParticipantsOfTheBall{" +
                "name='" + name + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
