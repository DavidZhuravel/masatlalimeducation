package lesson_8;

import java.util.Scanner;

public class MyLibrary {

    public static void main(String[] args) {
        HomeLibrary myLibrary = new HomeLibrary();

        String name;
        String genre;
        String author;
        int date;
        while (true) {
            Scanner in = new Scanner(System.in);
            System.out.println("Choice the option:");
            System.out.println("1) Add");
            System.out.println("2) Search book");
            System.out.println("3) Delete");
            System.out.println("4) Show All books");
            System.out.println("5) Sort");
            System.out.println("6) Exit");


            byte a = in.nextByte();

            switch (a){

                case 1:
                    System.out.println("Input name of book:");
                    name = in.next();
                    System.out.println("Input genre og book:");
                    genre = in.next();
                    System.out.println("Input name of author");
                    author = in.next();
                    System.out.println("Input year of publishing");
                    date = in.nextInt();
                    myLibrary.add(new Book(name,genre,author,date));
                    break;


                case 2:
                    System.out.println("Choice the option:");
                    System.out.println("1) Search by name/author/genre");
                    System.out.println("2) Search by date of publishing");
                    a = in.nextByte();
                    if (a == 1) {
                        System.out.println("Input name/author/genre of book:");
                        name = in.next();
                        myLibrary.showBook(name);
                    }
                    if (a == 2){
                        System.out.println("Input date of publishing of book:");
                        date = in.nextInt();
                        myLibrary.showBook(date);
                    }
                    break;

                case 3:
                    System.out.println("Choice the option:");
                    System.out.println("1) Delete by name/author/genre");
                    System.out.println("2) Delete by date of publishing");
                    a = in.nextByte();
                    if (a == 1) {
                        System.out.println("Input name/author/genre of book:");
                        name = in.next();
                        myLibrary.remove(myLibrary.getIndex(name));
                    }
                    if (a == 2){
                        System.out.println("Input date of publishing of book:");
                        date = in.nextInt();
                        myLibrary.remove(myLibrary.getIndex(date));
                    }
                    break;

                case 4:
                    System.out.println("------------------------");
                    myLibrary.showAllBook();
                    System.out.println("------------------------");
                    break;


                case 5:
                    System.out.println("Choice the option:");
                    System.out.println("1) Sort by name");
                    System.out.println("2) Sort by author");
                    System.out.println("3) Sort by genre");
                    System.out.println("4) Sort by date of publishing");
                    a = in.nextByte();
                    if (a == 1) myLibrary.sortByName();

                    if (a == 2) myLibrary.sortByAuthor();

                    if (a == 3) myLibrary.sortByGenre();

                    if (a == 4) myLibrary.sortByDateOfPublishing();
                    break;

                case 6:
                    System.exit(0);
            }
        }
    }
}
