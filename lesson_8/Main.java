package lesson_8;

public class Main {

    public static void main(String[] args) {

        String[] lastName = new String[]{"Абрамович","Головин","Щупак","Зильберман","Шевченко","Мартыненко", "Гоголь","Перес","Гранже","Твен",}; // array of last name
        int[] numOfGroup = new int[]{41,42,43}; // array of num of group

        Student[] arrayOfStudent = new Student[10]; // array of students

        for (int i = 0; i< arrayOfStudent.length; i++){ // create students
            arrayOfStudent[i] = new Student(lastName[i],numOfGroup[(int)(Math.random()*numOfGroup.length)]);
            arrayOfStudent[i].setAcademicPerformance();
            arrayOfStudent[i].showInfo();
        }
        System.out.println("----------");
        BubbleSort(arrayOfStudent); // sorted students

        for (int i = 0; i< arrayOfStudent.length; i++){
          //  arrayOfStudent[i].showInfo();// show all sorted students
            arrayOfStudent[i].topStudent();

        }
    }

    public static void swapElem(int firstNum, int secondNum, Student[] mass){
        Student tmpNum;
        tmpNum = mass[firstNum];
        mass[firstNum] = mass[secondNum];
        mass[secondNum] = tmpNum;
    }

    public static void BubbleSort(Student[] array){
        for (int i = array.length-1; i >= 1; i--) {
            for (int j = 0; j < i ; j++)
            {
                if (array[j].getGradePointAverage()>array[j+1].getGradePointAverage()){
                    swapElem(j, j+1, array);
                }
            }
        }
    }
}
