package lesson_8;

public class Number {

    private int firstNum;
    private int secondNum;


    public int getFirstNum() {
        return firstNum;
    }

    public void setFirstNum(int firstNum) {
        this.firstNum = firstNum;
    }

    public int getSecondNum() {
        return secondNum;
    }

    public void setSecondNum(int secondNum) {
        this.secondNum = secondNum;
    }

     void showNum(int firstNum, int secondNum){
        System.out.println(String.format("First num: %d\tSecond num: %d\n", firstNum, secondNum));
    }

    void sumOfNum(int firstNum, int secondNum){
        int sum = firstNum + secondNum;
        System.out.println(String.format("Your sum: %d",sum));
    }

    void  maxOfNum(int firstNum, int secondNum){
        if (firstNum>secondNum) System.out.println(String.format("The max num is: %d",firstNum ));
        if (firstNum<secondNum) System.out.println(String.format("The max num is: %d",firstNum ));
        else System.out.println( "both numbers are equal ");
    }
}
