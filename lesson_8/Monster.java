package lesson_8;

public class Monster {

    private String name;
    private int health;
    private int power;
    private int protection;

    Monster(String name, int health, int power, int protection){
        this.name = name;
        if (health > 0) this.health= health;
        if (power > 0) this.power= power;
        if (protection > 0) this.protection = protection;
    }
}
