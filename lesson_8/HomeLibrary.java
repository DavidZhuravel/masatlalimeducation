package lesson_8;

public class HomeLibrary{

    private Book[] books = new Book[1];
    private int pointer = 0;

    public void add(Book book){
        if (pointer == books.length-1){
            resize(books.length + 1);
        }
        books[pointer++] = book;
    }

    public Book getBook(int index){
        Book book = null;
        try {
            book = books[index];
        }
        catch (ArrayIndexOutOfBoundsException o){
            System.out.println("Book not found");
        }
        return book;
    }

    public void remove(int index){
        for (int i = index; i < pointer; i++)
            books[i] = books[i+1];
        books[pointer] = null;
        pointer--;
        if (books.length > 1 && pointer < books.length)
            resize(books.length-1);
    }

    public int getIndex(String name){
        int index = -1;

        for (int i = 0; i < pointer; i++)
            if (books[i].getName().equals(name)) index = i;
        return index;
    }

    public int getIndex(int date){
        int index = -1;

        for (int i = 0; i < pointer; i++)
            if (books[i].getYearOfPublishing() == date) index = i;


        return index;
    }

    public void showBook(String name){
        boolean flag = true;
        for (int i = 0; i < pointer; i++)
        {
            if (books[i].getName().equals(name) || books[i].getGenre().equals(name) || books[i].getAuthor().equals(name) )
                System.out.println(String.format("%d) The book %s of %s is the %s write on %d\n", i+1,books[i].getName(), books[i].getAuthor(), books[i].getGenre(), books[i].getYearOfPublishing()));
            flag = false;
        }if(flag) System.out.println("Not found the book");
    }

    public void showBook(int date){
        boolean flag = true;
        for (int i = 0; i< pointer; i++) {
            if (books[i].getYearOfPublishing() == date){
                System.out.println(String.format("%d) The book %s of %s is the %s write on %d\n",i+1, books[i].getName(), books[i].getAuthor(), books[i].getGenre(), books[i].getYearOfPublishing()));
                flag = false;
            }if(flag) System.out.println("Not found the book");
        }
    }

    public void showAllBook(){
        boolean flag = true;
        for (int i = 0; i< pointer; i++) {
            System.out.println(String.format("%d) The book %s of %s is the %s write on %d\n",i+1, books[i].getName(), books[i].getAuthor(), books[i].getGenre(), books[i].getYearOfPublishing()));
            flag = false;
        }if(flag) System.out.println("Not found the book");
    }

    private void resize(int num){
        Book[] newArrayOfBooks = new Book[num];
        System.arraycopy(books,0,newArrayOfBooks,0,pointer);
        books = newArrayOfBooks;
    }

    public void sortByName(){
        for (int i = 0; i < pointer; i++){
            for (int j = i; j < pointer-1; j++) {
                for (int k = 0; k < books[j].getName().length(); k++) {
                    if ( (int)books[j].getName().toLowerCase().charAt(k) == (int) books[j+1].getName().toLowerCase().charAt(k)) continue;
                    else if ((int)books[j].getName().toLowerCase().charAt(k) > (int) books[j+1].getName().toLowerCase().charAt(k)){
                        Book tmp = books[j];
                        books[j] = books[j+1];
                        books[j+1] = tmp;
                        break;
                    }
                }
            }
        }
    }

    public void sortByAuthor(){
        for (int i = 0; i < pointer; i++){
            for (int j = i; j < pointer-1; j++) {
                for (int k = 0; k < books[j].getAuthor().length(); k++) {
                    if ( (int)books[j].getAuthor().toLowerCase().charAt(k) == (int) books[j+1].getAuthor().toLowerCase().charAt(k)) continue;
                    else if ((int)books[j].getAuthor().toLowerCase().charAt(k) > (int) books[j+1].getAuthor().toLowerCase().charAt(k)){
                        Book tmp = books[j];
                        books[j] = books[j+1];
                        books[j+1] = tmp;
                        break;
                    }
                }
            }
        }
    }

    public void sortByGenre(){
        for (int i = 0; i < pointer; i++){
            for (int j = i; j < pointer-1; j++) {
                for (int k = 0; k < books[j].getGenre().length(); k++) {
                    if ( (int)books[j].getGenre().toLowerCase().charAt(k) == (int) books[j+1].getGenre().toLowerCase().charAt(k)) continue;
                    else if ((int)books[j].getGenre().toLowerCase().charAt(k) > (int) books[j+1].getGenre().toLowerCase().charAt(k)){
                        Book tmp = books[j];
                        books[j] = books[j+1];
                        books[j+1] = tmp;
                        break;
                    }
                }
            }
        }
    }

    public void sortByDateOfPublishing(){
        for (int i = 0; i < pointer; i++){
            for (int j = i; j < pointer-1; j++) {
                if (books[j].getYearOfPublishing() >  books[j+1].getYearOfPublishing()){
                        Book tmp = books[j];
                        books[j] = books[j+1];
                        books[j+1] = tmp;
                        break;

                }
            }
        }
    }
}
