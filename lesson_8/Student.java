package lesson_8;

import java.util.Scanner;

public class Student {

    private String lastName;
    private String initials;
    private int numOfGroup;
    private double gradePointAverage;
    private double[] academicPerformance = new double[5];

    Student(String lastName, int numOfGroup){
        this.lastName = lastName;
        this.numOfGroup = numOfGroup;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public int getNumOfGroup() {
        return numOfGroup;
    }

    public void setNumOfGroup(int numOfGroup) {
        this.numOfGroup = numOfGroup;
    }

    public double getGradePointAverage() {
        byte count = 0;
        double sum = 0;
        for(double num: academicPerformance){
            count++;
            sum += num;

        }
        return sum/count;
    }

    public void setGradePointAverage() {
        this.gradePointAverage = gradePointAverage;
    }

    public double[] getAcademicPerformance() {
        return academicPerformance;
    }

    public void setAcademicPerformance() {
        //System.out.println(lastName+" оценки:");
       // String[] subject = new String[]{"Математика","Физика","Химия","Английский","Литература",};
      //  Scanner in = new Scanner(System.in);
        for (int i = 0; i < academicPerformance.length; i++) {
           // System.out.print(String.format("%s:", subject[i]));
            academicPerformance[i] = (Math.random() * 2)+3;
           // System.out.println();

        }
    }


    public void showInfo(){
        System.out.print(String.format("Last name: %s\tNum of group: %d\tAverage point: %,.2f\n",getLastName(),getNumOfGroup(), getGradePointAverage()));
    }

    public void topStudent(){
        boolean flag = true;
        point:
        for (double num : academicPerformance)
            if (num < 4){
            flag = false;
            break point;
            }
            if (flag) showInfo();
    }


}
