package lesson_8;

public class Book {

    private String name;
    private String genre;
    private String author;
    private int yearOfPublishing;

    Book( String name, String genre, String author, int yearOfPublishing){
        this.name = name;
        this.genre = genre;
        this.author = author;
        this.yearOfPublishing = yearOfPublishing;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(int yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    public String getName() {
        return name;

    }

    public void setName(String name) {
        this.name = name;
    }


}
