package train.Serialization;

import java.io.*;

public class MainSerialization {

    public static void main(String[] args) {

        //Human[] humans = getData("D:\\MyFiles\\Java\\Test\\Train\\Serialization\\humans.txt");
Human[] humans = {new Human(23,"Jack")};
        for (int i = 0; i < humans.length; i++) {

            System.out.println(humans[i]);
        }

        saveData(humans, "D:\\MyFiles\\Java\\Test\\Train\\Serialization\\humans.txt");
    }

    public static Human[] getData(String url){

        Human[] human = null;
        try (FileInputStream inputStream = new FileInputStream(url)){
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

                human = (Human[]) objectInputStream.readObject();

            return human;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return human;
    }

    public static void saveData(Human[] human, String url){

        try (FileOutputStream outputStream = new FileOutputStream(url)) {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(human);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
