package train.Serialization;

import java.io.Serializable;

public class Human implements Serializable {

    private transient Dog dog = new Dog("Bucky");
    private int age;
    private String name;

    public Human(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name: '" + name +
                "', age: " + age + '}';
    }
}
