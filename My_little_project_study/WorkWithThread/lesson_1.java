package train.WorkWithThread;

public class lesson_1 {

    public static void main(String[] args) {

        Thread thread1 = new Thread(new MyThread(),"Stig");
        Thread thread2 = new Thread(new MyThread(), "Walley");
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName());
    }


}
