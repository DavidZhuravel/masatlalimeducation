package train.WorkWithThread;

public class MyThread implements Runnable{


    @Override
    public void run(){
        System.out.println(Thread.currentThread().getName() + " start work");
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " end work");
    }
}
