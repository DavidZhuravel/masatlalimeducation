package train;

public class BitCounting {

    public static byte bitCouting(int num){
        byte count = 0;
        while (num>1){
            count += num%2;
            num = (num - num%2)/2;
        }
        count += num;
        return count;
    }


}
