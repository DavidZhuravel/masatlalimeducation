package train.ClientServerGameNoughtsAndCrosses;



import javax.swing.*;
import java.awt.*;


public class GameGui {

    Button[] buttonList = new Button[9];
    JFrame frame;


    public void go(){

        frame = new JFrame("Noughts And Crosses");
        JPanel panel = new JPanel();

        for (int i = 0; i < 9; i++) {

            buttonList[i] = new Button(new JButton(""));
            Font bigFont = new Font("serif", Font.BOLD, 120);
            buttonList[i].setFont(bigFont);
buttonList[i].setText("x");


            panel.add(buttonList[i]);

        }
        frame.getContentPane().add(BorderLayout.CENTER,panel);
        frame.setSize(400,600);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public class Button extends JButton{

        JButton button;
        private boolean isActive;


        public Button(JButton button) {
            this.button = button;
        }

        public void setActive() {
            isActive = true;
        }

        public boolean isActive() {
            return isActive;
        }
    }
}
