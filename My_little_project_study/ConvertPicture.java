package train;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ConvertPicture {

    public static byte[] getData(String url){
        byte[] arrayBitOfPicture = null;
        try (FileInputStream inputStream = new FileInputStream(url)) {
             arrayBitOfPicture = new byte[inputStream.available()];
            inputStream.read(arrayBitOfPicture,0,arrayBitOfPicture.length);
            return arrayBitOfPicture;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayBitOfPicture;
    }

    public static void saveData(byte[] arrayBt, String url){

        try (FileOutputStream outputStream = new FileOutputStream(url)) {
            outputStream.write(arrayBt,0,arrayBt.length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        byte[] picture = getData("D:\\MyFiles\\Java\\Test\\Train\\ConvertMusic\\in.mp3");
        byte[] picture2 = getData("D:\\MyFiles\\Java\\Test\\Train\\ConvertMusic\\in2.mp3");
        System.out.println(picture.length);
        System.out.println(picture2.length);
        for (int i = picture.length/3+1500; i < (picture2.length); i++) {
            picture[i] = picture2[i];
        }

        saveData(picture,"D:\\MyFiles\\Java\\Test\\Train\\ConvertMusic\\out.mp3");
    }
}
