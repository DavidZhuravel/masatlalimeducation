package train.SocketsAndThreads;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.*;

public class DailyAdviceServer {

    String[] adviceList = {"go pepe", "drink beer", "call your girlfriend"};

    public void go(){

        try {

            ServerSocket serverSocket = new ServerSocket(5000);


                Socket socket = serverSocket.accept();

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                String advice = getAdvice();
                writer.write(advice);
                writer.flush();
               // System.out.println(advice);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getAdvice(){
        return adviceList[(int)(Math.random() * adviceList.length)];
    }

    public static void main(String[] args) {
        DailyAdviceServer server = new DailyAdviceServer();
        server.go();

    }
}
