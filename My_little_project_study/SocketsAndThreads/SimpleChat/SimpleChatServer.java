package train.SocketsAndThreads.SimpleChat;


import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;


public class SimpleChatServer {

    ArrayList clientOutputStreams;

    public class ClientHandler implements Runnable{
        BufferedReader reader;
        Socket socket;
        String[] names = {"John", "Monica", "Mike", "Jesica"};



        public ClientHandler(Socket socket) {
            try {

                this.socket = socket;
                InputStreamReader isReader = new InputStreamReader(socket.getInputStream());
                reader = new BufferedReader(isReader);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            String message;
            Thread.currentThread().setName(names[(int)(Math.random()*names.length)]);
            try {
                while ((message = reader.readLine())!= null){
                    System.out.println("read: " + message);
                    tellEveryone(Thread.currentThread().getName() + ": " + message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void go(){

        clientOutputStreams = new ArrayList();

        try {
            ServerSocket serverSocket = new ServerSocket(4242);

            while (true){
                Socket socket = serverSocket.accept();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                clientOutputStreams.add(writer);

                Thread t = new Thread(new ClientHandler(socket));
                t.start();
                System.out.println("You have got a connection");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void tellEveryone(String message){
        Iterator iterator = clientOutputStreams.iterator();
        while (iterator.hasNext()){
            try{
                BufferedWriter writer = (BufferedWriter) iterator.next();
                writer.write(  message + "\n");
                writer.flush();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }



    public static void main(String[] args) {
        new SimpleChatServer().go();

    }
}
