package train.SocketsAndThreads.Threads.BankOperation;

public class BankAccount {

    private  int balance = 100;

    public int getBalance() {
        return balance;
    }

    public void withDraw(int amount) {
        this.balance = balance - amount;
    }
}
