package train.SocketsAndThreads.Threads.BankOperation;

public class RyanAndMonicaJob implements Runnable {

    private BankAccount account = new BankAccount();


    public static void main(String[] args) {
        RyanAndMonicaJob job = new RyanAndMonicaJob();
        Thread one = new Thread(job);
        Thread two = new Thread(job);
        one.setName("Ryan");
        two.setName("Monica");
        one.start();
        two.start();
}

    @Override
    public void run() {
        int sum = 0;
       for (int i = 0; i< 10; i++){
            sum+= makeWithDraw(10);
            if (account.getBalance()<0) System.out.println("You have a problem!");
        }
        System.out.println(Thread.currentThread().getName() + " have " + sum + " dollars");
    }

    private int makeWithDraw(int amount){
        if (account.getBalance()>=amount){
            System.out.println(Thread.currentThread().getName() + " want to take money");
            try {
                System.out.println(Thread.currentThread().getName() + " go to sleep");
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " is awaking");
            account.withDraw(10);
            System.out.println(Thread.currentThread().getName() + " has end transaction");
        }else System.out.println("You haven`t money");

        return 10;
    }
}
