package train.SocketsAndThreads.Threads;

public class RunThreads implements Runnable {

    public static void main(String[] args) {
        Runnable runnable = new RunThreads();
        Thread alpha = new Thread(runnable);
        Thread beta = new Thread(runnable);
        alpha.setName("Alpha");
        beta.setName("Beta");
        alpha.start();
        beta.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 25; i++) {
            System.out.println("Now is working: " + Thread.currentThread().getName());
        }

    }
}
