package train.SocketsAndThreads;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class DaileAdviceClient {

    public void go(){

        try {
            Socket socket = new Socket("localhost",5000);
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String advice = reader.readLine();

            System.out.println("Today you must:" + advice);

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        DaileAdviceClient daileAdviceClient = new DaileAdviceClient();

        daileAdviceClient.go();
    }
}
