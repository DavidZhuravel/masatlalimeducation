package train.CardGame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

public class QuizCardGame {

    private JTextArea display;
    private JTextArea answer;
    private ArrayList<QuizCard> cardList;
    private QuizCard currentCard;
    private int currentCardIndex;
    private JFrame frame;
    private JButton nextButton;
    private boolean isShowAnswer;

    public static void main(String[] args) {
        QuizCardGame quizCardGame = new QuizCardGame();
        quizCardGame.go();
    }


    public void go(){
        //create frame

        frame = new JFrame("QuizCardGame");
        JPanel mainPanel = new JPanel();
        Font bigFont = new Font("sanserif",Font.BOLD,24);

        display = new JTextArea(6,20);
        display.setLineWrap(true);
        display.setWrapStyleWord(true);
        display.setFont(bigFont);

        JScrollPane dScroller = new JScrollPane(display);
        dScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        dScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        answer = new JTextArea(6,20);
        answer.setLineWrap(true);
        answer.setWrapStyleWord(true);
        answer.setFont(bigFont);

        nextButton = new JButton("Show Question");
        nextButton.addActionListener(new NextButtonListner());

        mainPanel.add(new JLabel("Question"));
        mainPanel.add(dScroller);
        mainPanel.add(new JLabel("Your answer"));
        mainPanel.add(answer);
        mainPanel.add(nextButton);

        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem gameMenu = new JMenuItem("Start Game");
        JMenuItem newMenuItem = new JMenuItem("CreateCard");
        JMenuItem saveMenuItem = new JMenuItem("Save");

        gameMenu.addActionListener(new GameMenuListner());
        newMenuItem.addActionListener(new NewCardListner());



        JLabel qLabel = new JLabel("Question:");
        JLabel aLabel = new JLabel("Answer");


        fileMenu.add(gameMenu);
        fileMenu.add(newMenuItem);
        fileMenu.add(saveMenuItem);
        menuBar.add(fileMenu);

        frame.setJMenuBar(menuBar);
        frame.getContentPane().add(BorderLayout.CENTER,mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(500,600);
        frame.setVisible(true);
    }

    public class GameMenuListner implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            currentCardIndex = 0;
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.showOpenDialog(frame);
            loadCard(jFileChooser.getSelectedFile());

        }
    }

    public class NewCardListner implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            QuizCardBuilder quizCardBuilder = new QuizCardBuilder();
            frame.setVisible(false);
            quizCardBuilder.go();
        }
    }

    public class NextButtonListner implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if (isShowAnswer){
                if (answer.getText().equals(currentCard.getAnswer()))
                    answer.setText("True");
                else answer.setText("False");
                nextButton.setText("Next question");
                isShowAnswer = false;
            }else if (currentCardIndex < cardList.size()){
                answer.setText("");
                showNextCard();
            }
            else {
                answer.setText("");
                display.setText("That was last card");
                nextButton.setEnabled(false);
            }
        }
    }

    private void loadCard(File file){

        cardList = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = reader.readLine())!= null){
                makeCard(line);
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex){
            ex.printStackTrace();
        }
        showNextCard();
    }

    public void makeCard(String lineToParse){

        String[] result = lineToParse.split("/");
        cardList.add(new QuizCard(result[1],result[0]));
    }

    public void showNextCard(){

        currentCard = cardList.get(currentCardIndex);
        currentCardIndex++;
        display.setText(currentCard.getQuestion());
        nextButton.setText("Confirm answer");
        isShowAnswer = true;
    }
}
