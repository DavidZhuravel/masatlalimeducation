package train.CardGame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class QuizCardBuilder {

    private JTextArea question;
    private JTextArea answer;
    private ArrayList<QuizCard> cardList;
    private JFrame frame;

    public static void main(String[] args) {
        QuizCardBuilder quizCardBuilder = new QuizCardBuilder();
        quizCardBuilder.go();
    }
    public void go(){
        // create frame

        frame = new JFrame("QuizCardBuilder");
        JPanel mainPanel = new JPanel();
        Font bigFont = new Font("sanserif", Font.BOLD,24);
        question = new JTextArea(6,20);
        question.setLineWrap(true);
        question.setWrapStyleWord(true);
        question.setFont(bigFont);

        JScrollPane qScroller = new JScrollPane(question);
        qScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        qScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        answer = new JTextArea(6,20);
        answer.setLineWrap(true);
        answer.setWrapStyleWord(true);
        answer.setFont(bigFont);

        JScrollPane aScroller = new JScrollPane(answer);
        aScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        aScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        JButton nextButton = new JButton("Next Card");

        cardList = new ArrayList<>();

        JLabel qLabel = new JLabel("Question:");
        JLabel aLabel = new JLabel("Answer");

        mainPanel.add(qLabel);
        mainPanel.add(qScroller);
        mainPanel.add(aLabel);
        mainPanel.add(aScroller);
        mainPanel.add(nextButton);

        nextButton.addActionListener(new NextCardListner());

        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem gameMenu = new JMenuItem("Start Game");
        JMenuItem newMenuItem = new JMenuItem("CreateCard");
        JMenuItem saveMenuItem = new JMenuItem("Save");

        gameMenu.addActionListener(new GameMenuListner());
        newMenuItem.addActionListener(new NewMenuListner());
        saveMenuItem.addActionListener(new SaveMenuListner());

        fileMenu.add(gameMenu);
        fileMenu.add(newMenuItem);
        fileMenu.add(saveMenuItem);
        menuBar.add(fileMenu);

        frame.setJMenuBar(menuBar);
        frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(500,600);
        frame.setVisible(true);
    }

    public class NextCardListner implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            cardList.add(new QuizCard(answer.getText(),question.getText()));
            clearCard();
        }
    }

    public class GameMenuListner implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            QuizCardGame quizCardGame = new QuizCardGame();
            frame.setVisible(false);
            quizCardGame.go();
        }
    }

    public class NewMenuListner implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            cardList.clear();
            clearCard();
        }
    }

    public class  SaveMenuListner implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if (!question.getText().equals("") && !answer.getText().equals("")){
                cardList.add(new QuizCard(answer.getText(), question.getText()));
            }
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.showSaveDialog(frame);
            saveFile(jFileChooser.getSelectedFile());
        }
    }

    public void saveFile(File file){

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            for (QuizCard quizCard : cardList) {
                writer.write(quizCard.getQuestion() + "/");
                writer.write(quizCard.getAnswer() + "\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clearCard(){
        question.setText("");
        answer.setText("");
        question.requestFocus();
    }
}
