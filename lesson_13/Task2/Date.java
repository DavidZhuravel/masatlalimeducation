package lesson_13.Task2;

import lesson_13.Task1.Day;
import lesson_13.Task1.Month;

public class Date {
    private Day dayOfWeek;
    private Month month;
    private int dayOfMonth;
    private int hours;
    private int minute;

    public Date(Day dayOfWeek, int dayOfMonth, Month month,  int hours, int minute) {
        this.dayOfWeek = dayOfWeek;
        this.dayOfMonth = dayOfMonth;
        this.month = month;
        this.hours = hours;
        this.minute = minute;
    }

    public Day getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Day day) {
        this.dayOfWeek = day;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

}
