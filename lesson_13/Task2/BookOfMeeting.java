package lesson_13.Task2;

import lesson_13.Task1.DateOfMeet;
import lesson_13.Task1.Day;
import lesson_13.Task1.Month;

import java.util.Scanner;

public class BookOfMeeting {

    private int year;
    Date[] dates;
    Month[] months = Month.values();
    Scanner in = new Scanner(System.in);

    public BookOfMeeting(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void init(int year){

        int countDay = 1;
        if (year%4==0) dates = new Date[366];
        else dates = new Date[365];
        System.out.println("Input first day of week in " + year);
        int indexDayOfWeek = indexOfDayInStart(in.next());
        for (int i = 1; i < months.length+1; i++){

            for (int j = countDay; j<countDay+ numOfDayInMonth(i,year); j++){


                dates[j-1] = new Date(Day.values()[indexDayOfWeek],j+1 - countDay,Month.values()[i-1],-1,-1);

               // System.out.println("month: " + Month.values()[i-1] + " day: " + (j+1 - countDay) + " "+Day.values()[indexDayOfWeek]);
                if (indexDayOfWeek == Day.values().length-1) indexDayOfWeek = 0;
                else indexDayOfWeek++;
            }
            countDay+=numOfDayInMonth(i,year);
        }
    } // инициализация календаря для задоного года

    private int numOfDayInMonth(int month ,int year){
        int num = 0;
        if ( month == 1 || month == 3 || month == 5|| month == 7|| month == 8|| month == 10|| month == 12) num+=31;
        if ( month == 4 || month == 6|| month == 9|| month == 11) num+=30;
        if (month == 2){
            if (year%4==0) num+=29;
            else num+=28;
        }
        return num;
    } // определяет сколько дней в текущем месяце текущего года

    private int indexOfDayInStart(String day){
        int index = 0;
        for (int i = 0; i <Day.values().length ; i++) {
            if (Day.values()[i].name().equals(day)) index = i;

        }
        return index;
    }

    public void show(){
        for (Date date : dates){
            if (date.getHours()!=-1 && date.getMinute()!=-1)
                System.out.printf("%d %s %d %s on %d:%d\n", year,date.getMonth(),date.getDayOfMonth(), date.getDayOfWeek(),date.getHours(),date.getMinute());
        }
    }

    public void addMeet(int day, String month){
        boolean flag = true;
        for (int i = 0; i <  dates.length ; i++) {
            if (dates[i].getMonth().name().equals(month) && dates[i].getDayOfMonth() == day && dates[i].getHours() == -1){
                System.out.println("Input hour of meet:");
                dates[i].setHours(in.nextInt());
                System.out.println("Input minute of meet:");
                dates[i].setMinute(in.nextInt());
                flag = false;
            }

        }
        if (flag) System.out.println("Oops! It`s date is not null");


    }

    @Override
    public int hashCode() {
        return (int)(Math.random()*99999999);
    }

    @Override
    public String toString() {
        return super.getClass().getSimpleName() ;
    }

}
