package lesson_13;

import lesson_13.Task1.DateOfMeet;
import lesson_13.Task1.Day;
import lesson_13.Task1.Month;
import lesson_13.Task2.BookOfMeeting;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        /*DateOfMeet[] dates = new DateOfMeet[1]; // Task1

        String day = null;
        String month = null;
        String name = null;
        String description = null;
        int hours = 0;
        int minute = 0;


        for (int i = 0; i < dates.length; i++) {
            System.out.println("Input day");
            day = in.next();
            System.out.println("Input month");
            month = in.next();
            System.out.println("Input name of meet");
            name = in.next();
            System.out.println("Input description");
            description = in.nextLine();
            System.out.println("Input hours");
            hours = in.nextByte();
            System.out.println("Input minute");
            minute = in.nextByte();

            dates[i] = new DateOfMeet(Day.valueOf(day), Month.valueOf(month),name,description,hours,minute);
        }

        for (DateOfMeet dayMeet: dates){
            dayMeet.show();
        }*/

        System.out.println("Input year of calendar book");
        BookOfMeeting book = new BookOfMeeting(in.nextInt());
        System.out.println(book.hashCode());
        System.out.println(book.toString());
        book.init(book.getYear());
        while (true) {
            System.out.println("----------------");
            System.out.println("1.Add meet");
            System.out.println("2.Show all meets");
            System.out.println("3.Exit");
            System.out.println("----------------");
            switch (in.nextByte()) {
                case 1:
                    System.out.println("Input day and month throw Enter(Example: 8 March)");
                    book.addMeet(in.nextInt(), in.next());
                    break;
                case 2:
                    book.show();
                    break;
                case 3:
                    System.exit(0);
            }
        }
    }
}
