package lesson_13.Task1;

public class DateOfMeet {
     private Day day;
     private Month month;
     private String name;
    private String description;
    private int hours;
    private int minute;

    public DateOfMeet(Day day, Month month, String name, String description, int hours, int minute) {
        this.day = day;
        this.month = month;
        this.name = name;
        this.description = description;
        this.hours = hours;
        this.minute = minute;
    }

     public void show(){
         System.out.printf("On %s of %s will be %s (%s) in time: %d:%d\n", day, month, name, description, hours, minute);
     }

}
