package lesson_19;

public abstract class Figure {

    public abstract void draw();
}
