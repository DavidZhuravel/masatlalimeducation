package lesson_19;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Figure> arrayList = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            int numOfFigure = (int) (Math.random()*4 +1);
            if (numOfFigure == 1) arrayList.add(new Cycle());
            else if (numOfFigure == 2) arrayList.add(new Hexagon());
            else if (numOfFigure == 3) arrayList.add(new Rhombus());
            else if (numOfFigure == 4) arrayList.add(new Square());
            else if (numOfFigure == 5) arrayList.add(new Triangle());
        }
        for (Figure figure : arrayList) {
            figure.draw();
            System.out.println();

        }
    }
}
