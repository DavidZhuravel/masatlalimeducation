package lesson_19;

public class Triangle extends Figure {

    @Override
    public void draw() {
        System.out.println("   *");
        System.out.println("  ***");
        System.out.println(" *****");
        System.out.println("*******");
    }
}
