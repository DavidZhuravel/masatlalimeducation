package lesson_19;

public class Hexagon extends Figure {

    @Override
    public void draw() {
        System.out.println("  ****  ");
        System.out.println(" ****** ");
        System.out.println("  ****  ");
    }
}
