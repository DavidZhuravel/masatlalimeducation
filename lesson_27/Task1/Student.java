package lesson_27.Task1;

import java.io.Serializable;
import java.util.Arrays;

public class Student implements Serializable {

    private String surName;
    private String name;
    private byte[] marks;
    private transient double avrMark;

    public Student(String surName, String name, byte[] marks) {
        this.surName = surName;
        this.name = name;
        this.marks = marks;
        setAvrMark();
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getMarks() {
        return marks;
    }

    public void setMarks(byte[] marks) {
        this.marks = marks;
    }

    public double getAvrMark() {
        return avrMark;
    }

    public void setAvrMark() {

        for (int i = 0; i < marks.length ; i++) {
            avrMark += (double) marks[i]/marks.length;
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "surName='" + surName + '\'' +
                ", name='" + name + '\'' +
                ", marks=" + Arrays.toString(marks) +
                ", avrMark=" + avrMark +
                '}';
    }
}
