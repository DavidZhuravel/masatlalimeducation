package lesson_27.Task1;


import javax.swing.*;
import java.io.*;
import java.util.ArrayList;

public class MainStudentSerialization {

    ArrayList<Student> list;


    public static void main(String[] args) {

        MainStudentSerialization studentSerialization = new MainStudentSerialization();
        studentSerialization.list = new ArrayList<>();
        studentSerialization.initialization();
        putData(studentSerialization.list);
      /* studentSerialization.list = (ArrayList<Student>) getData(new File("D:\\MyFiles\\Java\\Test\\Train\\Serialization\\student.txt"));
        for (Student student: studentSerialization.list) {
            System.out.println(student);
        }*/
    }

    private void initialization() {
        list.add(new Student("Zhuravel", "Dima", new byte[]{3, 4, 5}));
        list.add(new Student("Gofman", "Misha", new byte[]{5, 5, 5}));
        list.add(new Student("Hrizman", "Mark", new byte[]{5, 3, 4}));

        for (Student student : list) {
            System.out.println(student);

        }
    }

    public static void putData(ArrayList<? super Student> list) {
        JFrame frame = new JFrame("Save");

        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.showSaveDialog(frame);
        if (jFileChooser.getSelectedFile() != null) {
            try (ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(jFileChooser.getSelectedFile()))) {
                writer.writeObject(list);
                System.out.println("Data successfully saved");
            } catch (IOException e) {
                System.out.println("Error!");
            }
        }else System.exit(0);
    }

    public ArrayList<? super Student> getData() {

        JFrame frame = new JFrame("Load");
        ArrayList<? super Student> list = null;

        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.showOpenDialog(frame);

        if (jFileChooser.getSelectedFile()!= null) {
            try (ObjectInputStream reader = new ObjectInputStream(new FileInputStream(jFileChooser.getSelectedFile()))) {
                list = (ArrayList<? super Student>) reader.readObject();
                System.out.println("Download have been completed");
            } catch (ClassNotFoundException | IOException e) {
                System.out.println("Error!");
            }
            return list;
        }else System.exit(0);
        return null;
    }
}
