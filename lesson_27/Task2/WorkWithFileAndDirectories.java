package lesson_27.Task2;

/*Создать программу для работы с каталогами. На вход указать путь из которого начать. В
        программе должны быть функции показать список файлов и папок в текущей директории,
        создать/удалить каталог, вывести файлы определенного расширения.
*/

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class WorkWithFileAndDirectories {

    static String url = "";

    public static String cd(){
        String url = "";
        String tmpUrl = "";

       try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
           while (!(tmpUrl= reader.readLine()).equals("\\")){
               if (!tmpUrl.equals("..")) {
                   url += tmpUrl + "\\";
               }else {
                   String[] urls;
                   //urls = url.split("\");
               }

               showAllFilesAndDirectories(new File(url));
               System.out.println("******************************");
               System.out.print(url);
           }
       }catch (Exception ex) {ex.printStackTrace();}
       return url;
    }

    public static void showAllFilesAndDirectories(File file){

        for (String name : file.list())
            System.out.println(name);
    }

    public static void main(String[] args) {

        url = cd();
    }
}
