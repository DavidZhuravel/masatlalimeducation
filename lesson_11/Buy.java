package lesson_11;

public class Buy extends Product{

    private int count;


    public Buy(){}

    public Buy(String name, double price, double weight, int count) {
        super(name, price, weight);
        this.count = count;

    }

    public double getAllPrice() {
        return count*getPrice();
    }



    public double getAllWeight() {
        return  count * getWeight();
    }




    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
