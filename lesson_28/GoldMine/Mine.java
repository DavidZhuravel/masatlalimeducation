package lesson_28.GoldMine;

public class Mine {

    private int gold = 100;

    public synchronized int getGold() {
        System.out.println(Thread.currentThread().getName() + " say: I go to mine" );
        try {
            Thread.sleep((int)(Math.random()*100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " return from mine");
        if (gold>=10) {
            gold-=10;
            return 10;
        }else System.out.println("Gold is ending");
        return 0;
    }
    public boolean hasGold(){
        if (gold>0) return true;
        return false;
    }
}
