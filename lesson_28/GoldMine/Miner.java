package lesson_28.GoldMine;

public class Miner implements Runnable {

    Mine goldMine = new Mine();
    Castle homeCastle = new Castle();
    private int gold;

    @Override
    public void run() {

        while (goldMine.hasGold()) {
            gold = goldMine.getGold();
            if (gold != 0) homeCastle.addGold(gold);
            else {
                System.out.println(Thread.currentThread().getName() + " didn`t find any gold");
                break;
            }
        }
    }
}
