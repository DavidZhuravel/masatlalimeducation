package lesson_28.GoldMine;

public class Castle {

    private int gold;

    public synchronized void addGold(int gold){

        System.out.println(Thread.currentThread().getName() + ":THE NUMBER OF GOLD IN THE CASTLE INCREASED BY " + gold + "! Now there are a " + this.gold + " gold in the castle");

        this.gold += gold;
        try {
            Thread.sleep((int)(Math.random()*100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " return from castle");
    }


}
