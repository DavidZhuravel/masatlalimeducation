package lesson_28.GoldMine;

public class MainTestMiner {

    public static void main(String[] args) {

        Miner miner = new Miner();
        Thread oneMiner = new Thread(miner,"Jack");
        Thread twoMiner = new Thread(miner,"Bill");
        Thread threeMiner = new Thread(miner,"John");
        oneMiner.start();
        twoMiner.start();
        threeMiner.start();

    }
}
