package lesson_28;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Mine mine = new Mine();
        Thread first = new Thread(new Workers(mine));
        Thread second = new Thread(new Workers(mine));
        Thread third = new Thread(new Workers(mine));
        first.start();
        second.start();
        third.start();
    }
}

class Mine {
    private int gold;

    Mine() {
        gold = 100;
    }

    synchronized public int getGold() {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        gold -= 10;
        System.out.println("In mine now " + gold);
        return 10;
    }

    public boolean isEmpty() {
        if (gold > 0)
            return false;
        return true;
    }
}

class Workers implements Runnable {
    Mine mine;
    Random random;
    int gold;

    Workers(Mine mine) {
        this.mine = mine;
        random = new Random();
        gold = 0;
    }

    @Override
    public void run() {
        while (!mine.isEmpty()) {
            System.out.println(Thread.currentThread().getName() +
                    ": I go into mine!");
            gold += mine.getGold();
            System.out.println(Thread.currentThread().getName() +
                    "I have: " + gold);
            try {
                Thread.sleep((random.nextInt(3) + 1) * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName() +
                    ": Returned from mine!");
        }
    }
}
