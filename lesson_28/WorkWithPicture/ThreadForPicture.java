package lesson_28.WorkWithPicture;

public class ThreadForPicture implements Runnable {

    Picture picture = new Picture(10);

    @Override
    public void run() {
        while (picture.hasPixel()){
            Pixel pixel = picture.getPixel();
            if (pixel != null) {
                pixel.setValue((int)(Math.random()*9));
                picture.putPixel(pixel);
            }else {
                System.out.println("Not find pixel");
                break;
            }

            picture.showPicture();
        }


    }
}
