package lesson_28.WorkWithPicture;

public class Picture {

    private int count;
    private Pixel[] pixelsArr;

    public Picture(int size) {
        pixelsArr = new Pixel[size];

        for (int i = 0; i < size; i++) {
            pixelsArr[i] = new Pixel(i, 0);
        }
    }

    public Pixel[] getPixelsArr() {
        return pixelsArr;
    }

    public synchronized Pixel getPixel(){

        if (hasPixel()){
            System.out.println(Thread.currentThread().getName() + " takes the " + count + " pixel");
            Pixel pixel = pixelsArr[count];
            count++;
            try {
                Thread.sleep((int)(Math.random()*100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return pixel;
        }else return null;
    }

    public  void putPixel(Pixel pixel){
        System.out.println(Thread.currentThread().getName() + " put pixel into " + pixel.getPosition() + " position with value: " + pixel.getValue());
        pixelsArr[pixel.getPosition()] = pixel;
    }

    public boolean hasPixel(){
        if (count <= pixelsArr.length - 1) return true;
        return false;
    }

    public synchronized void showPicture(){
        for (int i = 0; i < pixelsArr.length; i++) {
            System.out.print(pixelsArr[i].getValue() + " ");
        }
        System.out.println();
    }
}