package lesson_28.WorkWithPicture;

public class Main {

    public static void main(String[] args) {

        ThreadForPicture thread = new ThreadForPicture();
        Thread one = new Thread(thread, "one");
        Thread two = new Thread(thread, "two");
        Thread three = new Thread(thread, "three");

        one.start();
        two.start();
        three.start();

    }
}
