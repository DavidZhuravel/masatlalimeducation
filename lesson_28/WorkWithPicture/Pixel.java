package lesson_28.WorkWithPicture;

public class Pixel {

    private int position;
    private int value;

    public Pixel(int position, int value) {
        this.position = position;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getPosition() {
        return position;
    }
}
