package lesson_7;

import lesson_5.ArrayTaskPart1;

public class Lesson7Task {

    public static void main(String[] args) {
        String[] abc = new String[]{"a","b","c","d","e","f","g","h","j"};
        int[] massInt = new int[]{4,1,6,-9,1,3};

        double[] mass1D = new double[]{4,5,2,-9,0,2,0,6};
        double[][] mass2D = new double[][]{{1,12,4},{4,0,11},{7,8,9}};
        double[][][]mass3D = new double[][][]{{{1, -2, 3}, {2, 10, 4}, {5, 2, -11}}, {{-4, -5, 6}, {1, 0, 8}, {-4, 2, 3}}};
        /*max(massInt);
        min(massInt);
        sumAfterOne(massInt);
        minInMainDiagonalOfSquareMatrix(mass2);
        random3DMatrix(3,3,3,5);
        equality(mass);
        sumOfPositiveNum(mass);
        increaseNumInArray(mass);
        threeHighestNumbersInArray(mass);
        ArrayTaskPart1.showMatrix(mass);*/
        // coupOfMatrix(mass);
        // shiftOfMatrix(mass,2);
       /* maxNumofMatrix(mass);
        minNumofMatrix(mass);
        notEquality(mass);*/
        //countOfEqualNum(mass);
       /* numOfElemEqualToZero(mass);
        numOfElemEqualToK(mass,1);
        normalizeMatrix(mass);*/
       /*sumOf2DMatrix(mass2);
       avarageOf2DMatrix(mass2);*/
        // sumOfElemInMainAndSecondaryDiagonal(mass2);
        // normalize2DMatrix(mass2);
        //divideMatrixByTwo(mass2D);
        // sumElemOf3DMatrix(mass3D);
        // changeSignIn3DMatrix(mass3D);
        // averageOfPositiveNumIn3D(mass3D);
        // sortAscending3DMatrix(mass3D);
        //sortBySumInXY(mass3D);
        //outputElemOf3DArrayIntoAStringThroughSpace(mass3D);
        //maxNumOfXYPlane(mass3D);
        //showElemOfXYPlate(mass3D);
        averageOfXYPlate(mass3D);
        //sort3DMatrixByAverageOfXYPlate(mass3D);
    }

    public final static void max(int[] mass){
        int max = mass[0];
        for (int num: mass){
            if (num > max) max = num;
        }
        System.out.println("Max:" + max);
    }

    public final static void min(int[] mass){
        int min = mass[0];
        for (int num: mass){
            if (num < min) min = num;
        }
        System.out.println("Min:" + min);
    }

    public final static void sumAfterOne(int[] mass){
        int sum = 0;
        int count = 0;
        boolean flag = false;
        for (int num: mass){
            if (num == 1) {
                flag = true;
                count++;
                continue;
            }else if (flag) {
                sum+=num;
                flag = false;
            }

        }
        if(count<2) sum = 0;
        System.out.println("Sum num after one:" + sum);
    }

    public final static void minInMainDiagonalOfSquareMatrix(double[][] mass){
        double min = mass[0][0];
        for (int i = 1; i<mass.length; i++){
            if (min > mass[i][i])
                min = mass[i][i];
        }
        System.out.println("Min num in main diagonal of matrix: " + min);
    }

    public static void random3DMatrix(int x, int y, int z){
        double[][][] array3D = new double[x][y][z];

        while (x>0){
            while (y>0){
                while (z>0){
                    array3D[x-1][y-1][z-1] = 1;
                    z--;
                }
                y--;
            }
            x--;
        }
    }

    public static void equality(double[] mass){
        boolean flag = true;
        for (double num: mass){
            if (mass[0]!= num) {
                flag =false;
                break;
            }
        }
        System.out.println("All num is equals: " + flag);
    }

    public static void sumOfPositiveNum(double[] mass){
        double sum  = 0;
        for (double num: mass){
            if (num>0) sum += num;
        }
        System.out.println("Sum of positive num: " + sum);
    }

    public static void increaseNumInArray(double[] mass){
        boolean flag = true;
        double buf = mass[0];
        for (double num: mass){
            if (buf>num) {
                flag =false;
                break;
            }
        }
        System.out.println("All num is increase: " + flag);
    }

    public static void threeHighestNumbersInArray(double[] mass){

        System.out.print("Array of three highest num: ");
        for (int i = 0; i < 3 ; i++) {
            for (int j = 1; j < mass.length ; j++) {
                double tmpNum;
                if (mass[j-1]> mass[j] ){
                    tmpNum = mass[j-1];
                    mass[j-1] = mass[j];
                    mass[j] = tmpNum;
                }

            }
            System.out.print(mass[mass.length-1-i] + "; ");
        }
        System.out.println();
    }

    private static void coupOfMatrix(double[] mass){
        double tmpNum;
        for (int i = 0; i < mass.length/2; i++) {
            tmpNum = mass[i];
            mass[i] = mass[mass.length-1-i];
            mass[mass.length-1-i] = tmpNum;
        }
    }

    private static void shiftOfMatrix(double[] mass, int k){
        for (int i = 0; i < k; i++) {
            double tmpNum = mass[0];
            double tmpNumNext;
            mass[0] = mass[mass.length-1];
            for (int j = 1; j < mass.length; j++) {
                tmpNumNext = mass[j];
                mass[j] = tmpNum;
                tmpNum = tmpNumNext;


            }

        }
    }

    private static void maxNumofMatrix(double[] mass){
        double max=0;
        for (double num: mass){
            if(max<num) max = num;
        }
        System.out.println("Max num of array: " + max);
    }

    private static void minNumofMatrix(double[] mass){
        double min=0;
        for (double num: mass){
            if(min>num) min = num;
        }
        System.out.println("Min num of array: " + min);
    }

    private static void notEquality(double[] mass) {
        boolean flag = true;
        for (int i = 0; i < mass.length; i++) {

            for (int j = 0; j< mass.length; j++) {
                if (mass[i] == mass[j] && j!=i) {
                    flag =false;
                    break;
                }
            }
        }
        System.out.println("All num is not equals: " + flag);
    }

    private static void countOfEqualNum(double[] mass){
        int count = 0;
        for (int i = 0; i < mass.length; i++) {
            boolean flag = true;
            for (int j = 0; j< mass.length; j++) {
                if (mass[i] == mass[j] && j!=i) {
                    flag = false;
                    break;
                }
            }
            if (flag) count++;
        }
        System.out.println("Num of equal num in array: " + count);
    }

    private static void numOfElemEqualToZero(double[] mass){
        int count = 0;
        for ( double num: mass)
            if (num == 0) count++;

        System.out.println("Number of elements equal to zero: " + count);
    }

    private static void numOfElemEqualToK(double[] mass, double k){
        int count = 0;
        for ( double num: mass)
            if (num == k) count++;

        System.out.println("Number of elements equal to k: " + count);

    }

    public static void normalizeMatrix(double[] mass){

        double sum = 0;
        for (double num: mass)
            sum += num;

        for (int i = 0; i < mass.length ; i++) {
            if (mass[i]>= 0)
                mass[i] = mass[i]/sum;
            System.out.print(mass[i] + "; ");
        }
        System.out.println();
    }

    public static void sumOf2DMatrix(double[][] mass){

        double sum = 0;
        for (int i = 0; i < mass.length ; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                sum += mass[i][j];
            }
        }
        System.out.println("Sum of 2D matrix: " + sum);
    }

    public static void avarageOf2DMatrix(double[][] mass){

        double avSum = 0;
        int count = 0;
        for (int i = 0; i < mass.length ; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                avSum += mass[i][j];
                count++;
            }
        }
        avSum /= count;
        System.out.println("Sum of 2D matrix: " + avSum);
    }

    public static void sumOfElemInMainAndSecondaryDiagonal(double[][] mass){

        double sumMain = 0;
        double sumSecondary = 0;

        for (int i = 0; i < mass.length; i++) {
            sumMain+=mass[i][i];
            sumSecondary += mass[i][mass.length-1-i];
        }
        System.out.println("Sum of element in main diagonal: " + sumMain);
        System.out.println("Sum of element in secondary diagonal: " + sumSecondary);
    }

    public static void normalize2DMatrix(double[][] mass){

        double max = 0;
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                if (max < Math.abs(mass[i][j])) max = mass[i][j];
            }
        }
        System.out.println("Your normalize matrix: ");
        for (int m = 0; m < mass.length; m++) {
            for (int n = 0; n < mass[m].length; n++) {
                mass[m][n] /= max;
                System.out.print(String.format("%,.2f",mass[m][n]) + " ");
            }
            System.out.println();
        }
    }

    public static void divideMatrixByTwo(double[][] mass){
        System.out.println("Matrix after divide by two:");
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                if (Math.abs(mass[i][j])> 10) mass[i][j]/=2;
                System.out.print(mass[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void show3DMatrix(double[][][] mass){

        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                for (int k = 0; k < mass[i][j].length; k++)
                {
                    System.out.print(mass[i][j][k] + " ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    public static void sumElemOf3DMatrix(double[][][] mass){
        double sum = 0;
        for (int i = 0; i <mass.length ; i++) {
            for (int j = 0; j < mass[i].length ; j++) {
                for (int k = 0; k < mass[i][j].length; k++) {
                    sum+=mass[i][j][k];
                }
            }
        }
        System.out.println("Sum of 3D matrix: " + sum);
    }

    public static void changeSignIn3DMatrix(double[][][] mass){

        for (int i = 0; i <mass.length ; i++) {
            for (int j = 0; j < mass[i].length ; j++) {
                for (int k = 0; k < mass[i][j].length; k++) {
                    if (mass[i][j][k] < 0) mass[i][j][k] *= -1;
                    System.out.print(mass[i][j][k] + " ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    public static void averageOfPositiveNumIn3D(double[][][] mass){
        int count = 0;
        double averageSum = 0;
        for (int i = 0; i <mass.length ; i++) {
            for (int j = 0; j < mass[i].length ; j++) {
                for (int k = 0; k < mass[i][j].length; k++) {
                    if (mass[i][j][k] > 0) {
                        averageSum+= mass[i][j][k];
                        count ++;
                    }
                }
            }
        }
        System.out.println("Average of positive elements on 3D matrix: " + String.format("%,.2f",averageSum / count));
    }

    //  public static void sortAscending3DMatrix(double[][][] mass){

    public static void swapElem(int firstNum, int secondNum, double[] mass){
        double tmpNum;
        tmpNum = mass[firstNum];
        mass[firstNum] = mass[secondNum];
        mass[secondNum] = tmpNum;
    }

    public static void improvedBubbleSort(double[] mass, double[][][] array){ // the method sorts an array and according to the sorting changes the position of the XY plane
        double tmpNum;
        for (int i = mass.length-1; i >= 1; i--) {
            for (int x = 0; x < i ; x++)
            {
                if (mass[x]>mass[x+1]){
                    swapElem(x, x+1, mass);
                    for (int y = 0; y < array[x].length ; y++) {
                        for (int z = 0; z < array[x][y].length; z++){
                            tmpNum = array[x][y][z];
                            array[x][y][z] = array[x+1][y][z];
                            array[x+1][y][z] = tmpNum;

                        }
                    }
                }
            }
        }
    }

    public static void sortBySumInXY(double[][][] mass){
        double[] sumXY = new double[mass.length];

        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                for (int k = 0; k < mass[i][j].length; k++)
                {
                    sumXY[i] += mass[i][j][k];
                }
            }
        }

        improvedBubbleSort(sumXY, mass);

        show3DMatrix(mass);
    }

    public static void outputElemOf3DArrayIntoAStringThroughSpace(double[][][] mass){
        String line = null ;
        for (int i = 0; i < mass.length ; i++) {
            for (int j = 0; j < mass[i].length; j++) {
                for (int k = 0; k < mass[i][j].length; k++) {
                   // System.out.print(mass[i][j][k] + " ");
                    if (i == 0 && j == 0 && k == 0) line = mass[i][j][k] + " ";

                    line += mass[i][j][k] + " ";
                }
            }
        }
        System.out.print(line);
    }

    public static void maxNumOfXYPlane(double[][][] mass){

        double[][] result = new double[mass[0].length][mass[0][0].length];
        int indexOfMax = 0;
        double max = mass[0][0][0];
        for (int i = 0; i < mass.length ; i++) {
            for (int j = 0; j < mass[i].length ; j++) {
                for (int k = 0; k < mass[i][j].length ; k++) {
                    if (max< mass[i][j][k]){
                        max = mass[i][j][k];
                        indexOfMax = i;
                    }
                }
            }
        }
        for (int i = 0; i < result.length ; i++) {
            for (int j = 0; j <result.length ; j++) {
                result[i][j] = mass[indexOfMax][i][j];
            }
        }
        ArrayTaskPart1.showMatrix(result);
    }

    public static void showElemOfXYPlate(double[][][] mass){

        for (int i = 0; i < mass.length; i++) {
            System.out.print("z: "+i + ") ");
            for (int j = 0; j < mass[i].length; j++) {
                for (int k = 0; k < mass[i][j].length; k++) {
                    System.out.print( mass[i][j][k] + "; ");
                }
            }
            System.out.println();
        }
    }

    public static void averageOfXYPlate(double[][][] mass){

        System.out.print("Average of each XY: ");
        for (int i = 0; i < mass.length ; i++) {
            int count = 0 ;
            double sum = 0;
            for (int j = 0; j < mass[i].length; j++) {
                for (int k = 0; k < mass[i][j].length; k++) {
                    sum += mass[i][j][k];
                    count++;
                }
            }
            System.out.print( String.format("%,.2f", sum/count) + "; ");
        }
        System.out.println();
    }

    public static void sort3DMatrixByAverageOfXYPlate(double[][][] mass){

        double[] averageMass = new double[mass.length];

        for (int i = 0; i < mass.length; i++) {
            int count = 0;
            for (int j = 0; j < mass[i].length; j++) {
                for (int k = 0; k < mass[i][j].length; k++) {
                    averageMass[i] += mass[i][j][k];
                    count++;
                }
            }
            averageMass[i]/=count;
        }

        improvedBubbleSort(averageMass,mass);

        show3DMatrix(mass);
    }
}


