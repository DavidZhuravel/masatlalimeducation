package lesson_5;

public class ArrayTaskPart1 {
    public static void main(String[] args) {
        double[] mass1 = new double[] {1,3,-1,2,4};
        double[][] mass2 = new double[][] {{2,5,7},{1,4,6}, {7,6,4}};
        double[][] mass3 = new double[][] {{1,2,3,5}, {4,5,6,5}, {7,7,9,5}, {5,7,9,4}};

        System.out.println("------------------");
        showMatrix(matrixTransposition(mass2));
        minNum(mass1);
        maxNum(mass1);
        sumOfMass(mass1);
        average(mass1);
        multiMass(mass1);
        sumOfOddNum(mass1);
        sumOfEvenNum(mass1);
        multiOfOddNum(mass1);
        multiOfEvenNum(mass1);
        numOfZeros(mass1);
        numOfOddNums(mass1);
        numOfEvenNums(mass1);
        numOfGreaterNumsThenNumK(mass1,5);
        numOfLessNumsThenNumK(mass1,0);
        numClosestToZero(mass1);
        normArray(mass1);
        minTwoDimensionalArray(mass2);
        maxTwoDimensionalArray(mass2);
        sumOfNumInTwoDimensionalArray(mass2);
        averageSumOfNumInTwoDimensionalArray(mass2);
        sumOfNumInMainDiagonalOfArray(mass2);
        sumOfNumInSecondaryDiagonalOfArray(mass2);
        normalizeTwoDimensionalArray(mass2);
        inverseMatrix(mass3);

    }

    private static double minNum(double[] mass){
        double minNum = mass[0];
        for (double tmpMinNum : mass){
            if (tmpMinNum < minNum)
                minNum = tmpMinNum;
        }
        System.out.println("Min num of mass is: " + minNum);

        return minNum;
    }

    private static double maxNum(double[] mass){
        double maxNum = mass[0];
        for (double tmpMaxNum : mass){
            if (tmpMaxNum > maxNum)
                maxNum = tmpMaxNum;
        }
        System.out.println("Max num of mass is: " + maxNum);

        return maxNum;
    }

    private static double sumOfMass(double[] mass){
        double sum = 0;
        for (int i = 0; i<mass.length; i++){
            sum+=mass[i];
        }
        System.out.println("Sum of mass: " + sum);

        return sum;
    }

    private static double average(double[] mass){
        double averageMass = 0;
        for (int i = 0; i<mass.length; i++) {
            averageMass += mass[i];
        }
        averageMass /= mass.length;
        System.out.println("Average of mass: " + averageMass);

        return averageMass;
    }

    private static double multiMass(double[] mass){
        int multiMass = 1;
        for (int i = 0; i< mass.length; i++){
            multiMass *= mass[i];
        }
        System.out.println("Multi of mass: " + multiMass);

        return multiMass;
    }

    public static double sumOfOddNum(double[] mass){
        double sumOddNum = 0;
        for (int i = 0; i< mass.length; i++){
            if (i%2!=0)
                sumOddNum += mass[i];
        }
        System.out.println("Sum of odd num: " + sumOddNum);

        return sumOddNum;
    }

    public static double sumOfEvenNum(double[] mass){
        double sumEvenNum = 0;
        for (int i = 0; i< mass.length; i++){
            if (i%2 == 0)
                sumEvenNum += mass[i];
        }
        System.out.println("Sum of even num: " + sumEvenNum);

        return sumEvenNum;
    }

    public static double multiOfOddNum(double[] mass){
        double multiOddNum = 1;
        for (int i = 0; i< mass.length; i++){
            if (i%2!=0)
                multiOddNum *= mass[i];
        }
        System.out.println("Sum of odd num: " + multiOddNum);

        return multiOddNum;
    }

    public static double multiOfEvenNum(double[] mass){
        double multiEvenNum = 1;
        for (int i = 0; i< mass.length; i++){
            if (i%2 == 0)
                multiEvenNum *= mass[i];
        }
        System.out.println("Sum of even num: " + multiEvenNum);

        return multiEvenNum;
    }

    public static  byte numOfZeros(double[] mass){
        byte count = 0;
        for (double num : mass){
            if (num == 0) count++;
        }
        System.out.println("Num of zeros in the array: " + count);

        return count;
    }

    public static  byte numOfOddNums(double[] mass){
        byte count = 0;
        for (double num : mass){
            if (num%2!= 0) count++;
        }
        System.out.println("Num of odd numbers in the array: " + count);

        return count;
    }

    public static  byte numOfEvenNums(double[] mass){
        byte count = 0;
        for (double num : mass){
            if (num%2 == 0) count++;
        }
        System.out.println("Num of even numbers in the array: " + count);

        return count;
    }

    public static  byte numOfGreaterNumsThenNumK(double[] mass, int k){
        byte count = 0;
        for (double num : mass){
            if (num > k) count++;
        }
        System.out.println("Num of greater numbers then number k in the array: " + count);

        return count;
    }

    public static  byte numOfLessNumsThenNumK(double[] mass, int k){
        byte count = 0;
        for (double num : mass){
            if (num < k) count++;
        }
        System.out.println("Num of less numbers then number k in the array: " + count);

        return count;
    }

    private static double numClosestToZero(double[] mass){
        double numClosestToZero = mass[0];
        for (int i = 0; i< mass.length; i++){
            if (i ==0) numClosestToZero = mass[i];
            if (Math.abs(mass[i])< numClosestToZero)
                numClosestToZero = mass[i];
        }
        System.out.println("The number closest to zero is:" + numClosestToZero);

        return numClosestToZero;
    }

    public static  double[] normArray(double[] mass){
        double maxNum = mass[0];
        for (double tmpMaxNum : mass){
            if (tmpMaxNum > maxNum)
                maxNum = tmpMaxNum;
        }
        System.out.print("Your normalize array: ");
        for (int i = 0; i< mass.length; i++){
            mass[i]/=maxNum;
            System.out.print(mass[i]);
        }
        System.out.println();

        return mass;
    }

    public static double minTwoDimensionalArray(double[][] mass){
        double minNum = mass[0][0];
        for (int i =0; i<mass.length; i++){
            for (int j = 0; j<mass[i].length; j++){
                if (mass[i][j]< minNum)
                    minNum = mass[i][j];
            }
        }
        System.out.println("Min num of two-dimensional array is: " + minNum);

        return minNum;
    }

    public static double maxTwoDimensionalArray(double[][] mass){
        double maxNum = mass[0][0];
        for (int i =0; i<mass.length; i++){
            for (int j = 0; j<mass[i].length; j++){
                if (mass[i][j]> maxNum)
                    maxNum = mass[i][j];
            }
        }
        System.out.println("Min num of two-dimensional array is: " + maxNum);

        return maxNum;
    }

    public static double sumOfNumInTwoDimensionalArray(double[][] mass){
        double sum = 0;
        for (int i =0; i<mass.length; i++){
            for (int j = 0; j<mass[i].length; j++){
                sum += mass[i][j];
            }
        }
        System.out.println("Sum of num in two-dimensional array is: " + sum);

        return sum;
    }

    public static double averageSumOfNumInTwoDimensionalArray(double[][] mass){
        double averageSum = 0;
        int count = 0;
        for (int i =0; i<mass.length; i++){
            for (int j = 0; j<mass[i].length; j++){
                averageSum += mass[i][j];
                count++;
            }
        }
        averageSum /=count;
        System.out.println(" Average sum of num in two-dimensional array is: " + averageSum);

        return averageSum;
    }

    public static double sumOfNumInMainDiagonalOfArray(double[][] mass){
        double sumMainDiagonal = 0;
        if ( mass.length!=mass[0].length) {
            System.out.println("Error! Non-square matrix!");
        }else {
            for (int i = 0; i < mass.length; i++){
                sumMainDiagonal+= mass[i][i];
            }
            System.out.println("Sum of num in main diagonal of square matrix: " + sumMainDiagonal);
        }
        return sumMainDiagonal;
    }

    public static double sumOfNumInSecondaryDiagonalOfArray(double[][] mass){
        double sumSecondaryDiagonal = 0;
        if ( mass.length!=mass[0].length) {
            System.out.println("Error! Non-square matrix!");
        }else {
            for (int i = 0; i < mass.length; i++){
                sumSecondaryDiagonal+= mass[i][mass.length-1-i];
            }
            System.out.println("Sum of num in secondary diagonal of square matrix: " + sumSecondaryDiagonal);
        }
        return sumSecondaryDiagonal;
    }

    public static double[][] normalizeTwoDimensionalArray(double[][] mass){
        double maxNum = mass[0][0];

        for (int i =0; i<mass.length; i++){
            for (int j = 0; j<mass[i].length; j++){
                if(maxNum < mass[i][j])
                    maxNum = mass[i][j];
            }
        }
        System.out.println("Your normalize array: ");
        for (int i =0; i<mass.length; i++){
            for (int j = 0; j<mass[i].length; j++){
                mass[i][j]/=maxNum;
                System.out.print(String.format("%,.2f",mass[i][j]) + " ");
            }
            System.out.println();
        }

        return mass;
    }

    public static double[][] matrixTransposition(double[][] mass) {
        double[][] massTransp = new double[mass[0].length][mass.length];
        for (int i = 0; i < massTransp.length; i++) {
            for (int j = 0; j < massTransp[i].length; j++) {
                massTransp[i][j] = mass[j][i];
            }
        }
        return  massTransp;
    }

    public static  double matrix2x2Determinant(double[][] mass){
        double result = 0;
        if (mass.length!= mass[0].length){
            System.out.println("Error! Non-square matrix!");
        }else {
            result = mass[0][0] * mass[1][1] - mass[0][1]*mass[1][0];
        }

        return result;
    }

    public static double[][] minorOfMatrix(double[][] mass){           // матрица миноров
        double [][] tmpMass = new double[mass.length][mass.length];
        for (int i = 0; i< mass.length; i++){
            for (int j = 0; j < mass.length; j++){
                if(mass.length < 3)
                    tmpMass[i][j] = matrixDeterminant(mass);
                tmpMass[i][j] = matrixDeterminant(minor(i,j,mass));
            }
        }
        return tmpMass;
    }

    public static double[][] algebraicComplements(double[][] mass){
        for (int i = 1; i <= mass.length; i++){
            for (int j = 1; j<= mass.length; j++){
                if ((i+j)%2!=0)
                    mass[i-1][j-1] *= -1;
            }
        }
        return mass;
    }

    public static double[][] inverseMatrix(double [][] mass){

        if (mass.length!= mass[0].length){
            System.out.println("Error! Non-square matrix!");
        }else{
            double determinant = matrixDeterminant(mass);
            double[][] bufMass;
            bufMass = matrixTransposition(algebraicComplements(minorOfMatrix(mass)));
            System.out.println("Your inverse matrix: ");
            for (int i = 0; i < mass.length; i++){
                for (int j = 0; j < mass.length; j++){
                    mass[i][j] = (1/determinant) * bufMass[i][j];
                    System.out.print(String.format("%,.2f",mass[i][j]) + " ");
                }
                System.out.println();
            }
        }
        return mass;
    }

    public static  double matrixDeterminant (double[][] mass){

        double determinant = 0;
        double tmpDeterminant = 0;

        if (mass.length!= mass[0].length){
            System.out.println("Error! Non-square matrix!");
        }else{
            if (mass.length == 2){
                tmpDeterminant = matrix2x2Determinant(mass);
            }else{
                for (int i = 0; i<mass[0].length; i++){
                    determinant += Math.pow(-1, i) *mass[i][0]* matrixDeterminant(minor(i, 0, mass));

                }
                tmpDeterminant = determinant;
            }
        }
        return  tmpDeterminant;
    }

    public static double[][] minor(int m, int n, double[][] mass){ // обрезка матрицы
        double[][] tmpMass = new double[mass.length-1][mass.length-1];
        int iTmpMass = 0;
        int jTmpMass;
        for (int i = 0; i < mass.length; i++) {
            jTmpMass = 0;
            if(i == m) continue;
            for( int j = 0; j<mass.length; j++){
                if (j == n) continue;
                tmpMass[iTmpMass][jTmpMass] = mass[i][j];
                jTmpMass++;
            }
            iTmpMass++;
        }
        return tmpMass;
    }

    public static void showMatrix(double[][] mass){
        for (int i = 0; i<mass.length; i++){

            for (int j = 0; j< mass[0].length; j++){
                System.out.print(mass[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void showMatrix(double[] mass){
        for (int i = 0; i<mass.length; i++){

                System.out.print(mass[i] + " ");
            }
            System.out.println();

    }
}
