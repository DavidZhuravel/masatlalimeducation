package lesson_16;

import java.util.Scanner;

public class Task1 {

    public static double task1(){
        Scanner in = new Scanner(System.in);
        System.out.println("Input num:");
        double result = in.nextInt();
        try {
            if (result == 0) throw new Exception("Number = 0");
            result = 1/result;
        } catch (Exception ex ){
            System.out.println(ex.getMessage());
            task1();
        }
        return result;
    }

    public static double task2(){
        Scanner in = new Scanner(System.in);
        System.out.println("Input the point");
        double result = in.nextDouble();
        try {
            if(result<=-71 || result>=14) throw new Exception("out of boundaries");
            result = 12*result+44;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            task2();
        }
        return result;
    }

    public  static  double task3(int left, int right){
        Scanner in = new Scanner(System.in);
        System.out.println("Input the point");
        double result = in.nextDouble();
        try {
            if(result<=left || result>=right) throw new Exception("out of boundaries");
            result = 18*Math.pow(result,2) + (54/result) - 8;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            task3(left,right);
        }
        return result;

    }
}
