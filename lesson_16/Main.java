package lesson_16;

import lesson_16.Game.Computer;
import lesson_16.Game.Game;
import lesson_16.Game.Human;

import static lesson_16.Task1.*;

public class Main {
    public static void main(String[] args) {
      //  System.out.println(task1());
      //  System.out.println(task2());
       // System.out.println(task3(-5,5));
        Game game = new Game(new Human(), new Computer());
        game.play();
    }
}
