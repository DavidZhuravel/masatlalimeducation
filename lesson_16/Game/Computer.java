package lesson_16.Game;

public class Computer implements Player {

    private String[] nameArr = {"Champ", "Lucky", "Tramp"};
    private String[] decisionArr = {"rock", "paper", "scissors"};
    private String name;
    private String decision;
    private int points;



    public void setDecision() {
        decision = decisionArr[(int)(Math.random() * decisionArr.length)];
    }


    public void setName() {
        this.name = nameArr[(int)(Math.random() * nameArr.length)];
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDicision() {
        return decision;
    }

    @Override
    public void addPoint() {
        points++;
    }

    @Override
    public int getPoints() {
        return points;
    }
}
