package lesson_16.Game;

import java.util.Scanner;

public class Human implements Player {

    private String name;
    private String decision;
    private int points;

    public void setName() {
        Scanner in = new Scanner(System.in);
        System.out.println("Input your name:");
        name = in.next();
    }


    @Override
    public String getName() {
        return name;
    }

    public void setDecision(){
        Scanner in = new Scanner(System.in);
        System.out.println("Input your decision:");
        try {
            decision = in.next();
            if (!decision.equals("rock") && !decision.equals("paper") && !decision.equals("scissors")) throw new Exception("Wrong decision");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            setDecision();
        }
    }
    @Override
    public String getDicision() {
        return decision;
    }

    @Override
    public void addPoint() {
         points++;

    }

    @Override
    public int getPoints() {
        return points;
    }
}
