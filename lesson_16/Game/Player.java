package lesson_16.Game;

public interface Player {


    String getName();
    String getDicision();
    void addPoint();
    int getPoints();

}
