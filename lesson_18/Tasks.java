package lesson_18;

import java.util.ArrayList;
import java.util.Scanner;

public class Tasks {
    static ArrayList<String> arrayList = new ArrayList<>();
    static Scanner in = new Scanner(System.in);

    public static void task1(){
        for (int i = 1; i < 11; i++) {
            arrayList.add("Sentence #" + i);
        }
        for (String s : arrayList) {
            System.out.println(s);
        }
    }

    public static void task2(){
        for (int i = 1; i < 11; i++) {
            System.out.println("Sentence #" + i);
            arrayList.add(in.next());
        }
        int maxSize = arrayList.get(0).length();
        for (int i = 1; i < arrayList.size() ; i++) {
            if ( maxSize < arrayList.get(i).length()) maxSize = arrayList.get(i).length();
        }
        for (String s : arrayList) {
            if (s.length() == maxSize) System.out.println(s);
        }
    }

    public static void task3() {
        for (int i = 1; i < 11; i++) {
            System.out.println("Sentence #" + i);
            arrayList.add(in.next());
        }
        int minSize = arrayList.get(0).length();
        for (int i = 1; i < arrayList.size(); i++) {
            if (minSize > arrayList.get(i).length()) minSize = arrayList.get(i).length();
        }
        for (String s : arrayList) {
            if (s.length() == minSize) System.out.println(s);
        }
    }

    public static void task4(String string){

        arrayList.add(0,string);
        for (String s : arrayList) {
            System.out.println(s);
        }
    }

    public static void task5(int shift){

        for (int i = 1; i < 11; i++) {
            arrayList.add("Sentence #" + i);

        }
        for (int i = 0; i < shift ; i++) {
            int newI = 1;
            String tmpString = arrayList.get(newI-1);
            for (int j = 0; j < arrayList.size(); j++) {
                if (j == arrayList.size() - 1 ) newI = 0;
                tmpString = arrayList.set(newI++, tmpString);
            }
        }
        for (String s : arrayList) {
            System.out.println(s);
        }
    }

    public static void task6(){
        for (int i = 0; i < 10; i++) {
            System.out.printf("Sentence #%d: \n", i+1);
            arrayList.add(in.next());
        }
        int indexOfMin = 0;
        int indexOfMax = 0;
        for (int i = 1; i < arrayList.size(); i++) {
            if (arrayList.get(indexOfMin).length() > arrayList.get(i).length()) indexOfMin = i;
            if (arrayList.get(indexOfMax).length() < arrayList.get(i).length()) indexOfMax = i;
        }
        if (indexOfMax<indexOfMin) System.out.println(arrayList.get(indexOfMax));
        else System.out.println(arrayList.get(indexOfMin));
    }
}
