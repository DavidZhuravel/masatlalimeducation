package UnplannedTasks2;

/*Числа, одинаково читающиеся слева направо и справа налево, называются палиндромами.
        Например, 1223221. Напишите программу нахождения всех палиндромов на данном отрезке.*/
public class Task4 {

    public static boolean isPolindrom(int num){
        char[] array = String.valueOf(num).toCharArray();
        if (array.length>1) {
            for (int i = 0; i < array.length/2; i++) {
                if (array[i]!= array[array.length-1-i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static void findPolindrom(int min, int max){
        for (int i = min; i < max+1; i++) {
            if (isPolindrom(i)) System.out.println(i + " is polindrom");
        }
    }

    public static void main(String[] args) {
        findPolindrom(2,1000000);
    }
}
