package UnplannedTasks2;

/*Найдите количество и сумму цифр в данном натуральном числе.*/
public class Task2 {

    private static int count = 0;

    public static int sumAndCountOfNumber(int num){
        count++;
          if (num<10) return num;
          return (num%10) + sumAndCountOfNumber((num - num%10)/10);
    }

    public static void main(String[] args) {
        int num = 1234;
        System.out.println("Sum numbers of number(" + num + ") is: " + sumAndCountOfNumber(num) + " | count of numbers: " + count);
    }
}
