package UnplannedTasks2;

/*  Если сложить все цифры какого-либо натурального числа, затем — все цифры найденной суммы и так далее,
        то в результате получим однозначное число (цифру), которое называется цифровым корнем данного числа.
        Например, цифровой корень числа 561 равен 3 (5 + 6+1 — 12, 1+2 = 3).
        Написать программу для нахождения числового корня данного натурального числа.*/

public class Task6 {

    public static int sumOfDigits(int num){
        if (num<10) return num;
        return num%10 + sumOfDigits((num - num%10) /10);
    }

    public static void digitalRoot(int num){
        System.out.printf("The digital root of number(%d): %d\n", num, sumOfDigits(sumOfDigits(num)));
    }

    public static void main(String[] args) {
        digitalRoot(5612);
    }
}
