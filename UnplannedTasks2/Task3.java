package UnplannedTasks2;

/*Дано натуральное число. Поменяйте в нем порядок цифр на обратный.*/

public class Task3 {

    public static String reverse( int num){
        if (num<10) return String.valueOf(num);
        return String.valueOf(num%10) + reverse((num - num%10)/10);
    }

    public static void main(String[] args) {
        System.out.println(reverse(123));
    }
}
