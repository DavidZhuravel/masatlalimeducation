package UnplannedTasks2;

/*Два нечетных простых числа, отличающиеся на 2, называются близнецами. Например, числа 5 и 7.
        Напишите программу, которая будет находить все числа-близнецы на отрезке [2; 1000].*/

public class Task1 {

    public static void twinsNumbers(int min, int max){

        for (int i = min; i < max -1 ; i++) {
            if (isSimple(i) && isSimple(i+2)) System.out.println("Twins numbers: " + i + " | " + (i+2));
        }
    }

    public static boolean isSimple(int num){
        for (int i = 2; i < num; i++) {
            if (num%i == 0) return false;
        }
        return true;
    }

    public static void main(String[] args) {
        twinsNumbers(2, 1000);
    }
}
