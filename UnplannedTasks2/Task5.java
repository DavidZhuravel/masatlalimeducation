package UnplannedTasks2;

/*Числа, запись которых состоит из двух одинаковых последовательностей цифр, называются симметричными.
        Например, 357357 или 17421742. Определите, является ли данное натуральное число симметричным.*/

import java.util.ArrayList;

public class Task5 {

    public static void mySymmetricalNumber(int num){

        char[] array = String.valueOf(num).toCharArray();
        ArrayList<Character> arrayList = new ArrayList<>();
        boolean flag = false;
        short count = 0;

        arrayList.add(0,array[0]);
        for (int i = 1; i < array.length; i++) {
            count = 0;
            if (array[i]!= arrayList.get(0)) arrayList.add(array[i]);
            else{
                for (int j = i; j < array.length; j++) {
                    if (count < arrayList.size()){
                        if (array[j] != arrayList.get(count)){
                           flag = true;
                           break;
                        }
                        count++;
                    }else count = 0;
                }
                if (flag || count< arrayList.size()) {
                    arrayList.add(array[i]);
                    flag = false;
                }
            }
        }
        if (arrayList.size() == array.length && count!=arrayList.size()) System.out.println("it isn`t symmetrical number");
        else System.out.println("it is symmetrical number");
    }

    public static void symmetricalNumber(int num){

        char[] array = String.valueOf(num).toCharArray();
        boolean flag = true;

        if (array.length%2==0){
            for (int i = 0; i < array.length/2; i++) {
                if (array[i]!= array[array.length/2+i]){
                    flag = false;
                    break;
                }
            }if (flag) System.out.println("it is symmetrical number");
            else System.out.println("it isn`t symmetrical number");

        }else System.out.println("it isn`t symmetrical number");
    }

    public static void main(String[] args) {
        mySymmetricalNumber(121212);
        symmetricalNumber(121212);
    }
}
