package lesson_2;

import java.util.Scanner;

public class Distance {

    public static final short speed_of_voice = 1100;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Input time of storm on seconds: ");
        float time = in.nextFloat();
        System.out.println("Distance: " + String.format("%,.2f", time*speed_of_voice ) + " ft");
    }
}
