package lesson_2;


import java.util.Scanner;

public class WeightOnMoon {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Input your mass:");
        float mass = in.nextFloat();
        System.out.println("Your mass on the Moon: " + String.format("%,.2f",mass*0.17 ) + "kg");
        System.out.println("Your weight on the Moon: " + String.format("%,.2f",mass*0.17*9.81 ) + "H");
    }
}
