package lesson_9;

public class Monster {

    private String name;
    private int health;
    private double power;
    private double protect;

    public Monster(String name, int health, double power, double protect) {
        this.name = name;
        this.health = health;
        this.power = power;
        this.protect = protect;
    }

    public final void show(){
        System.out.println("The " + this.name + " has  health: " + this.health + "; power: "+ this.power + "; protect: " + this.protect);
    }
}
