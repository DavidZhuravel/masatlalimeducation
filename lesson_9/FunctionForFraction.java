package lesson_9;

public class FunctionForFraction {

    public static void sum(Fraction one, Fraction two){

        double sum;
        if (one.getY() > two.getY()){
            if (one.getY()%two.getY()==0) sum = (one.getX() + two.getX()*(one.getY()/two.getY()))/one.getY();
            else sum = (one.getX()*two.getY() + two.getX()* one.getY())/(one.getY()*two.getY());
        }else if (one.getY() < two.getY()){
            if (one.getY()%two.getY()==0) sum = (one.getX()*(two.getY()/one.getY()) + two.getX())/two.getY();
            else sum = (one.getX()*two.getY() + two.getX()*one.getY())/(one.getY()*two.getY());
    }
    else sum = (one.getX() + two.getX())/one.getY();
        System.out.println(sum);
    }

    public static void sub(Fraction one, Fraction two){

        double sum;
        if (one.getY() > two.getY()){
            if (one.getY()%two.getY()==0) sum = (one.getX() - two.getX()*(one.getY()/two.getY()))/one.getY();
            else sum = (one.getX()*two.getY() - two.getX()* one.getY())/(one.getY()*two.getY());
        }else if (one.getY() < two.getY()){
            if (one.getY()%two.getY()==0) sum = (one.getX()*(two.getY()/one.getY()) - two.getX())/two.getY();
            else sum = (one.getX()*two.getY() - two.getX()*one.getY())/(one.getY()*two.getY());
        }
        else sum = (one.getX() - two.getX())/one.getY();
        System.out.println(sum);
    }

    public static double multi(Fraction one, Fraction two){
       return (one.getX()*two.getX())/(one.getY()*two.getY());
    }

    public static double division(Fraction one, Fraction two){
        return (one.getX()*two.getY())/(one.getY()*two.getX());
    }

    public static void showFraction(Fraction one){
        System.out.println("Your fraction is: " + one.getX() + "/" + one.getY());
    }

}
