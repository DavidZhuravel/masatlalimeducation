package lesson_9;
import static java.lang.Math.*;
public class Function {

    private double firstPower;
    private double secondPower;
    private double thirdPower;
    private double freeMember;

    public Function(double firstPower, double secondPower, double thirdPower, double freeMember) {
        this.firstPower = firstPower;
        this.secondPower = secondPower;
        this.thirdPower = thirdPower;
        this.freeMember = freeMember;
    }

    public final double resultOfFunction(double x){
        return pow(x,firstPower)+ pow(x,secondPower) + pow(x,thirdPower) + freeMember;
    }

    public final void showResult(double x){
        System.out.println("Result of x^" + firstPower + "+x^"+ secondPower + "+x^" + thirdPower + "+"+ freeMember+" when x = "+x + " is " + resultOfFunction(x));
    }
}
