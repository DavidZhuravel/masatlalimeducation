package lesson_9;

public class Fraction {

    private double x; // числитель
    private double y; // знаменатель

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    Fraction(double x, double y){
        this.x = x;
        this.y = y;
    }


}
