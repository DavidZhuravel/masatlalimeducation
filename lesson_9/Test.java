package lesson_9;

public class Test {

    public static void main(String[] args) {
        int[] array = new int[]{1,6,5,0,-1,5};
        int k = 5;
        countOfEvenNum(array);
        countOfNumLessThenK(array,k);

    }

    public static void countOfNumLessThenK(int[] array, int k){
        int count = 0;
        for (int i = 0; i < array.length ; i++) {
            if (array[i] <= k) count++;

        }
        System.out.println("Count num less then k:" + count);
    }

    public static  void countOfEvenNum(int[] array){
        int count = 0;
        for (int num: array){
            if (num%2!=0) count++;
        }
        System.out.println("Count of even num: " + count);
    }




}
