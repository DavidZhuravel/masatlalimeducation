package lesson_9;

import static java.lang.Math.pow;

public class Polynomial {

    private double firstPower;
    private double secondPower;
    private double thirdPower;
    private double forthPower;
    private double fifthPower;
    private double freeMember;

    public Polynomial(double firstPower, double secondPower, double thirdPower, double forthPower, double fifthPower, double freeMember) {
        this.firstPower = firstPower;
        this.secondPower = secondPower;
        this.thirdPower = thirdPower;
        this.forthPower = forthPower;
        this.fifthPower = fifthPower;
        this.freeMember = freeMember;
    }

    public final double resultOfFunction(double x) {
        return pow(x, firstPower) + pow(x, secondPower) + pow(x, thirdPower) + pow(x, forthPower) + pow(x, fifthPower) + freeMember;
    }

    public final double derivative(double x) {
        return pow((firstPower*x), (firstPower-1)) + pow((secondPower*x), (secondPower-1)) + pow((thirdPower*x), (thirdPower-1)) + pow((forthPower*x), (forthPower-1)) + pow((fifthPower*x), (fifthPower-1));
    }

}
