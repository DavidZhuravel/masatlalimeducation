package lesson_9;

public class Date {

    private int day;
    private int month;
    private int year;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public final void season(){
        if (month==1 || month==2 || month == 12){
            System.out.println("Winter");
        }else if (month>=3 && month<=5){
            System.out.println("Spring");
        }else if (month>=6 && month<=8){
            System.out.println("Summer");
        }else if (month>=9 && month<=11){
            System.out.println("Autumn");
        }else System.out.println("Wrong");
    }

    public final int numOfDayInYear(int month){
        int num = 0;
        for (int i = 1; i<month; i++){
            if ( i == 1 || i == 3 || i == 5|| i == 7|| i == 8|| i == 10|| i == 12) num+=31;
            if ( i == 4 || i == 6|| i == 9|| i == 11) num+=30;
            if (i == 2){
                if (year%4==0) num+=29;
                else num+=28;
            }
        }
        num+=getDay();
        System.out.println("This is a "+ num + " day in the year");
        return num;
    }

    public final void differenceInDates(Date date){
        int tmpMax;
        int tmpMin;
        int difference = 0;
        if(year> date.year){
            tmpMax = year;
            tmpMin = date.year;
        }else{
            tmpMin = year;
            tmpMax = date.year;
        }
        for (int i = tmpMin; i < tmpMax; i++){
            if(i%4==0) difference+=366;
            else  difference += 365;
        }
        difference+= Math.abs(numOfDayInYear(month) - date.numOfDayInYear(date.getMonth()));


        System.out.println("The difference between the dates in days are: " + difference);
            }
        }



