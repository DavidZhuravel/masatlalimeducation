package lesson_9;

public class Triangle {

    private double a;
    private double b;
    private double c;

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    Triangle(double a, double b, double c){

        setA(a);
        setB(b);
        setC(c);
    }

    public final double perimeter(){
        return getA()+getB()+getC();
    }

    public  final double squareByGeron(){
        double p = perimeter()/2;
        return Math.sqrt(p*(p-getA())*(p-getB())*(p-getC()));
    }

    public final String typeOfTriangle(){

        if (getA()==getB() && getB() == getC() && getA() == getC())
            return "Equilateral";
        else if ( getA()==getB() || getB() == getC() || getA() == getC())
            return "Isosceles";
        else return "Versatile";
    }
}
