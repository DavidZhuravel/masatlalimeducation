package lesson_9;

public class Figure {

    private double length;
    private double width;
    private double height;
    private String typeOfFigure;

    public Figure(double length) {
        this.length = length;
        typeOfFigure = "line";
    }

    public Figure(double length, double width) {
        this.length = length;
        this.width = width;
        if (length == width) typeOfFigure = "square";
        else typeOfFigure = "rectangle";
    }

    public Figure(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
        if (length == width && length == height) typeOfFigure = "cube";
        else typeOfFigure = "parallelepiped";
    }

    public final void showTypeOfFigure(){
        System.out.println("Type of your figure is " + typeOfFigure);
    }
}
