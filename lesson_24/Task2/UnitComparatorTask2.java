package lesson_24.Task2;

import java.util.Comparator;

public class UnitComparatorTask2 implements Comparator<Unit> {
    @Override
    public int compare(Unit o1, Unit o2) {
        if (o1.getHealght()>o2.getHealght()) return -1;
        else if (o1.getHealght() < o2.getHealght()) return 1;
        else return 0;
    }
}
