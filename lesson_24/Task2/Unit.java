package lesson_24.Task2;

public class Unit {

    private int health;

    public Unit(int health) {
        this.health = (int)((Math.random() * health/3)+ 3*health/4);
    }

    public int getHealght() {
        return health;
    }

    public void setHealght(int healght) {
        this.health = healght;
    }

    public boolean isDead(){
        if (health<=0) return true;
        return false;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "health=" + health +
                '}';
    }
}
