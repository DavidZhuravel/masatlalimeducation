package lesson_24.Task1;


public class Knight{

    private int honor;
    private String name;

    public Knight(int honor, String name) {
        this.honor = honor;
        this.name = name;
    }

    public int getHonor() {
        return honor;
    }

    public String getName() {
        return name;
    }


}
