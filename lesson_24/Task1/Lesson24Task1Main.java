package lesson_24.Task1;


import java.util.Map;
import java.util.TreeMap;

public class Lesson24Task1Main {

    public static void main(String[] args) {

        ComparatorKnight compr = new ComparatorKnight();

        TreeMap<Integer,Knight> treeMap = new TreeMap(compr);

        String[] arraysNames = {"Stiv", "Ostin", "Jack", "Colin"};
        Knight tmp;

        for (int i = 0; i < arraysNames.length; i++) {
            tmp = new Knight((int)(Math.random()*100),arraysNames[i]);
            treeMap.put(tmp.getHonor(),tmp);
        }




        for (Map.Entry<Integer,Knight> item : treeMap.entrySet()) {
            System.out.printf(" Name: %s  Honor: %d\n", item.getValue().getName(), item.getValue().getHonor());

        }
    }
}
