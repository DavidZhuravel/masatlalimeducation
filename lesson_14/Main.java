package lesson_14;
import lesson_14.lastTask.ElfArcher;
import lesson_14.lastTask.ElfMage;
import lesson_14.lastTask.ElfWarrior;
import lesson_14.lastTask.Unit;

import static lesson_14.Task2.*;



public class Main {

    public static void main(String[] args) {
        task1( new Integer(9));
        System.out.println(task2(new Integer(3)));
        System.out.println(task3(new Integer(3), new Integer(4)));
        System.out.println(task4(new Character('a')));

        Unit<ElfArcher> unit = new Unit(new ElfArcher());
        unit.show();
    }
}
