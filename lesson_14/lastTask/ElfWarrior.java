package lesson_14.lastTask;

public class ElfWarrior extends Elf {
    @Override
    public void strike() {
        System.out.println("I`m Elf Warrior");
    }

    @Override
    public String toString() {
        return getClass().getName();
    }
}
