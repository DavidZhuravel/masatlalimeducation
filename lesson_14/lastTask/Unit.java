package lesson_14.lastTask;

public class Unit<T extends Elf> {

    private T unit;

    public Unit(T unit) {
        this.unit = unit;
    }

    public void show(){
        unit.strike();
        unit.toString();

    }
}
