package lesson_14.lastTask;

public class ElfArcher extends Elf {
    @Override
    public void strike() {
        System.out.println("I`m Elf Archer");
    }

    @Override
    public String toString() {
        return getClass().getName();
    }
}
