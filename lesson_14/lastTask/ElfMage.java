package lesson_14.lastTask;

public class ElfMage extends Elf {
    @Override
    public void strike() {
        System.out.println("I`m Elf Mage");
    }

    @Override
    public String toString() {
        return getClass().getName();
    }
}
