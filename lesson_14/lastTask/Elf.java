package lesson_14.lastTask;

public abstract class Elf {

    private int health;
    private int damage;

    public abstract void strike();

}
