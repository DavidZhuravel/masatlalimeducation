package lesson_14;

public class Task2 {

    public static <T> void task1(T elem){
        System.out.println(elem.getClass().getSimpleName() + ":" + elem);
    }

    public static <T> boolean task2(T elem){
        boolean flag = false;
        if(elem instanceof Boolean){
            if (((Boolean) elem).booleanValue() == false) flag = true;
        }

        if (elem instanceof Integer){
            if (((Integer) elem).intValue() == 0) flag = true;
        }
        return flag;
    }

    public static <T> T task3(T elem1, T elem2){
        T res = null;
        if (elem1 instanceof Integer && elem2 instanceof Integer){
            Integer tmpRes = ((Integer) elem1).intValue()+ ((Integer) elem2).intValue();
            res = (T) tmpRes;

        }

        if (elem1 instanceof Double && elem2 instanceof Double){
            Double tmpRes = ((Double) elem1).doubleValue()+ ((Double) elem2).doubleValue();
            res = (T) tmpRes;

        }
        return res;
    }

    public static <T> int task4( T elem){
        int res = 0;
        if (elem instanceof String){
            for (int i = 0; i < ((String) elem).length() ; i++) {
                res += ((String) elem).charAt(i);
            }
        }

        if (elem instanceof Character) res = ((Character) elem).charValue();
        return res;
    }

    public static <T> void task5(T[] array){

        if (array instanceof Integer[]){
           Integer[] resArray = (Integer[]) array.clone();
        }

        if (array instanceof Double[]){
            Double[] resArray = (Double[]) array.clone();

        }

        if (array instanceof Boolean[]){
            Boolean[] resArray = (Boolean[]) array.clone();
        }
    }

}
