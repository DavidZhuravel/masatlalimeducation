package lesson_12;

import lesson_12.Game.Computer;
import lesson_12.Game.Game;
import lesson_12.Game.Human;
import lesson_12.Task3.Orc;
import lesson_12.Task3.OrcGrunt;

public class Main {

    public static void main(String[] args) {
       /* Rodent rat = new Rat(true,5,"Gray",10);
        rat.run();*/

       /*Orc orc = new OrcGrunt(92,50,100);
       orc.attack();*/

        Game game = new Game(new Human(),new Computer());
        game.play();
    }
}
