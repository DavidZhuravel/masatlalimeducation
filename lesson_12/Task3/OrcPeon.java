package lesson_12.Task3;

import lesson_12.Task3.Orc;

public class OrcPeon extends Orc {


    public OrcPeon(int attack, int protect, int health) {
        super(attack, protect, health, "crashing");
    }


    @Override
    public void attack() {
        System.out.println(getClass().getSimpleName() + " has a " + getAttack()+ " points of "+ getTypeOfAttack());
    }
}
