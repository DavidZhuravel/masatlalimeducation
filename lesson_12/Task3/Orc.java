package lesson_12.Task3;

public abstract class Orc {

    private int attack;
    private int protect;
    private int health;
    private String typeOfAttack;

    public Orc(int attack, int protect, int health, String typeOfAttack) {
        this.attack = attack;
        this.protect = protect;
        this.health = health;
        this.typeOfAttack = typeOfAttack;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getProtect() {
        return protect;
    }

    public void setProtect(int protect) {
        this.protect = protect;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public String getTypeOfAttack() {
        return typeOfAttack;
    }

    public void setTypeOfAttack(String typeOfAttack) {
        this.typeOfAttack = typeOfAttack;
    }

    public abstract void attack();
}
