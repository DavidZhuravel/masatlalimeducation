package lesson_12.Task3;

import lesson_12.Task3.Orc;

public class OrcShaman extends Orc {
    public OrcShaman(int attack, int protect, int health) {
        super(attack, protect, health, "magical");
    }

    @Override
    public void attack() {
        System.out.println(getClass().getSimpleName()+ " has a " + getAttack()+ " points of "+ getTypeOfAttack());
    }
}
