package lesson_12.Task3;

import lesson_12.Task3.Orc;

public class OrcGrunt extends Orc {

    public OrcGrunt(int attack, int protect, int health) {
        super(attack, protect, health, "hacking");
    }

    @Override
    public void attack() {
        System.out.println(getClass().getSimpleName()+ " has a " + getAttack()+ " points of "+ getTypeOfAttack());
    }
}
