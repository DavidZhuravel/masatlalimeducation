package lesson_12.Task2;

public abstract class Rodent {

    private boolean sex;
    private double weight;
    private String color;
    private double speed;

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public Rodent(boolean sex, double weight, String color, double speed) {
        this.sex = sex;
        this.weight = weight;
        this.color = color;
        this.speed = speed;
    }

    abstract void run();
    abstract void jump();
    abstract void eat();

}
