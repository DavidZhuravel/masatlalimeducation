package lesson_12.Task2;

public class Chinchilla extends Rodent {


    public Chinchilla(boolean sex, double weight, String color, double speed) {
        super(sex, weight, color, speed);
    }

    @Override
    void run() {
        System.out.println(getClass().getSimpleName() + " has  speed: " + getSpeed());
    }

    @Override
    void jump() {

    }

    @Override
    void eat() {

    }
}
