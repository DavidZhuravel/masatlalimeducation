package lesson_12.Game;

import java.util.Scanner;

public class Human implements Player {

    private String name;
    private String decision;
    private int points;

    public void setName() {
        Scanner in = new Scanner(System.in);
        System.out.println("Input your name:");
        name = in.next();
    }


    @Override
    public String getName() {
        return name;
    }

    public void setDecision(){
        Scanner in = new Scanner(System.in);
        System.out.println("Input your decision:");
        decision = in.next();
    }
    @Override
    public String getDicision() {
        return decision;
    }

    @Override
    public void addPoint() {
         points++;

    }

    @Override
    public int getPoints() {
        return points;
    }
}
