package lesson_12.Game;

public interface Player {


    String getName();
    String getDicision();
    void addPoint();
    int getPoints();

}
