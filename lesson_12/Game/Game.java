package lesson_12.Game;

import java.util.Scanner;

public class Game {

    Human player1;
    Computer player2;

    public Game(Human player1, Computer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public void play(){
        player1.setName();
        player2.setName();

        Scanner in = new Scanner(System.in);


        while (true) {
            System.out.println("Input number of the option:");
            System.out.println("1 - let`s game");
            System.out.println("2 - exit");
            switch (in.nextByte()){
                case 1:
                    player1.setDecision();
                    player2.setDecision();
                    System.out.println(player1.getDicision()  + " " + player2.getDicision());
                    if ((player1.getDicision().equals("paper") && player2.getDicision().equals("rock"))|| (player1.getDicision().equals("scissors") && player2.getDicision().equals("paper")) || (player1.getDicision().equals("rock") && player2.getDicision().equals("scissors"))) {player1.addPoint();
                    } else if(player1.getDicision().equals(player2.getDicision())) System.out.println("dead heat");
                    else player2.addPoint();
                    System.out.printf("%s has: %d points    %s has: %d points\n",player1.getName(),player1.getPoints(),player2.getName(),player2.getPoints());
                    break;

                case 2:
                    System.exit(0);
            }
        }
    }
}
