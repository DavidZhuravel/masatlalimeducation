package lesson_12.Task1;

public abstract class Dog {

    private String Name;
    private boolean sex;
    private double weight;

    public abstract void voice();
    public abstract void action();

}
