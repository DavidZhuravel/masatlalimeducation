package lesson_17_2;

public class LostConnection extends Exception {

    public String getError() {
        return connection;
    }

    private String connection;

    public LostConnection(String message, String connection){
        super(message);
        this.connection = connection;
    }
}
