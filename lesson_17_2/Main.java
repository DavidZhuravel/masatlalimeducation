package lesson_17_2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in =new Scanner(System.in);
        String con = in.next();
        try {
            new GetConnection(con);
        } catch (LostConnection lostConnection) {
            System.out.println( lostConnection.getMessage() + ": " + lostConnection.getError());
        }

    }
}
