package lesson_17_2;

public class GetConnection {
    String con;

    public String getCon() {
        return con;
    }

    public GetConnection(String con) throws LostConnection{
        if (con.equals("error")) throw new LostConnection("Don`t find the connection", con);
        else this.con = con;

    }


}
